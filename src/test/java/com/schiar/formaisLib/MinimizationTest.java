package com.schiar.formaisLib;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;

import com.schiar.formaisLib.algorithm.Minimizer;
import com.schiar.formaisLib.model.FiniteAutomaton;

public class MinimizationTest {
	
	@Test
	public void testMinimizationWithUnreachableAndDeadStates() {
		String[][] table = {new String[] {"e", "a", "b"},
				new String[] {"->S", "E", "G"},
				new String[] {"*A", "I", "H"},
				new String[] {"*B", "A", "B"},
				new String[] {"*C", "F", "I"},
				new String[] {"*D", "D", "C"},
				new String[] {"*E", "F", "G"},
				new String[] {"*F", "F", "C"},
				new String[] {"*G", "E", "H"},
				new String[] {"*H", "A", "H"},
				new String[] {"I", "I", "I"}};
		FiniteAutomaton automaton = new FiniteAutomaton("teste", "automato para minimizar", false, table);
		List<FiniteAutomaton> result = Minimizer.minimize(automaton);
		FiniteAutomaton minimized = result.get(result.size()-1);
		assertEquals(automaton.getStates().size()-3, minimized.getStates().size());
	}
	
	@Test
	public void testMinimizationWithStatesInTheSameEquivalenceClass() {
		String[][] table = {new String[] {"e", "a", "b"},
				new String[] {"*->A", "G", "B"},
				new String[] {"B", "F", "E"},
				new String[] {"C", "C", "G"},
				new String[] {"*D", "A", "H"},
				new String[] {"E", "E", "A"},
				new String[] {"F", "B", "C"},
				new String[] {"*G", "G", "F"},
				new String[] {"H", "H", "D"}};
		FiniteAutomaton automaton = new FiniteAutomaton("teste", "automato para minimizar", false, table); 
		List<FiniteAutomaton> result = Minimizer.minimize(automaton);
		FiniteAutomaton minimized = result.get(result.size()-1);
		assertEquals(3, minimized.getStates().size());
		assertEquals(1, minimized.getFinalStates().size());
		assertTrue(minimized.getInitialState().equals(minimized.getFinalStates().first()));
	}
	
	@Test
	public void testMinimizationWithNDFA() {
		String[][] table = {new String[] { "e", "0", "1" },
				new String[] { "->q0", "q0,q2", "q1"},
				new String[] { "*q1", "q1", "q2" },
				new String[] { "q2", "q0", "q1" }};
		FiniteAutomaton fa = new FiniteAutomaton("fa", "ndfa", true, table);
		List<FiniteAutomaton> result = Minimizer.minimize(fa);
		FiniteAutomaton minimized = result.get(result.size()-1);
		assertEquals(2, minimized.getStates().size());
		assertEquals(1, minimized.getFinalStates().size());
		assertTrue(fa.equals(minimized));
	}

}
