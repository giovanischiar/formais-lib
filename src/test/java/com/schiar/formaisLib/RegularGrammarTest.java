package com.schiar.formaisLib;

import static org.junit.Assert.*;
import org.junit.Test;

import com.schiar.formaisLib.model.Pair;
import com.schiar.formaisLib.model.RegularGrammar;

public class RegularGrammarTest {
	@Test
	public void testCreateGrammar() {
		String grammar = "S -> aA | bB\nA -> aS | a\nB -> bS | b";
		RegularGrammar g = new RegularGrammar(grammar);
		assertTrue(g.getNonTerminals().contains('S'));
		assertTrue(g.getNonTerminals().contains('A'));
		assertTrue(g.getNonTerminals().contains('B'));
		assertTrue(g.getNonTerminals().size() == 3);
		assertTrue(g.getTerminals().contains('a'));
		assertTrue(g.getTerminals().contains('b'));
		assertTrue(g.getTerminals().size() == 2);
		assertTrue(g.getProductionRules().contains(new Pair<String, Pair<Character, Character>>("S", new Pair<Character, Character>('a', 'A'))));
		assertTrue(g.getProductionRules().contains(new Pair<String, Pair<Character, Character>>("S", new Pair<Character, Character>('b', 'B'))));
		assertTrue(g.getProductionRules().contains(new Pair<String, Pair<Character, Character>>("A", new Pair<Character, Character>('a', 'S'))));
		assertTrue(g.getProductionRules().contains(new Pair<String, Pair<Character, Character>>("A", new Pair<Character, Character>('a', '&'))));
		assertTrue(g.getProductionRules().contains(new Pair<String, Pair<Character, Character>>("B", new Pair<Character, Character>('b', 'S'))));
		assertTrue(g.getProductionRules().contains(new Pair<String, Pair<Character, Character>>("B", new Pair<Character, Character>('b', 'S'))));
		assertTrue(g.getProductionRules().contains(new Pair<String, Pair<Character, Character>>("B", new Pair<Character, Character>('b', '&'))));
		assertTrue(g.getProductionRules().size() == 6);
	}
}
