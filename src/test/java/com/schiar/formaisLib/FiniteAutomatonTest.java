package com.schiar.formaisLib;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.LinkedList;

import org.junit.Before;
import org.junit.Test;

import com.schiar.formaisLib.model.FiniteAutomaton;

public class FiniteAutomatonTest {

	FiniteAutomaton fa;
	
	@Before
	public void setUp() throws Exception {
		String[][] table = {new String[] {"e", "a", "b", "c"},
        new String[] {"->q0", "q1", "q1", "q1"},
        new String[] {"*q1", "q0", "q0", "q0"}};
		fa = new FiniteAutomaton("teste", "palavras de tamanho �mpar", false, table); 
	}
	
	@Test
	public void testWithAllNSizeWordsThatAutomatonRecognize() {
		int n = 5;
		int sum = 0;
		LinkedList<String> sentences = fa.allSentencesFrom0ToNThatFARecognize(n);
		for(int i = 1; i <= n; i+=2) {
			sum+=Math.pow(fa.getAlphabet().size(), i);
		}
		assertEquals(sentences.size(), sum);
	}
	

	@Test
	public void testAutomatonWithWordThatBelongs() {
		assertTrue(fa.reconizeWord("aabccac"));
	}
	
	@Test
	public void testAutomatonWithWordThatDoesntBelongs() {
		assertFalse(fa.reconizeWord("abababbb"));
	}

	
	@Test
	public void testAutomatonWithEmptyWord() {
		assertFalse(fa.reconizeWord(""));
	}
	
	@Test
	public void testAutomatonWithWordOutOfAlphabet() {
		assertFalse(fa.reconizeWord("e"));
	}
	
	@Test
	public void testEquivalence() {
		String[][] table2 = {    new String[] { "e", "0", "1" },
				 			  new String[] { "->q0", "q0", "q1"},
				 			  new String[] { "*q1", "q1", "q0" }};
		
		     String[][] table3 = {new String[] { "e", "0", "1" },
	 			  			 new String[] { "->q0", "q0,q2", "q1"},
	 			  			 new String[] { "*q1", "q1", "q2" },
	 			  			 new String[] { "q2", "q0", "q1" }};
		FiniteAutomaton fa1 = new FiniteAutomaton("fa1", "n�mero �mpar de 1", false, table2);
		FiniteAutomaton fa2 = new FiniteAutomaton("fa2", "n�mero �mpar de 1", true, table3);
		assertTrue(fa1.equals(fa2));
	}
	
	@Test
	public void testRenameStates() {
		String[][] table = { new String[] { "e", "a" },
				 			  new String[] { "A", "A" },
				 			  new String[] { "q2", "q2" },
				 			  new String[] { "->{A,q2}", "A,q2" }};
		FiniteAutomaton fa = new FiniteAutomaton("fa", "teste", false, table);
		FiniteAutomaton newFA = fa.copyWithNewStateNames();
		assertTrue(fa.equals(newFA));
	}
	
}
