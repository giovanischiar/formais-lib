package com.schiar.formaisLib;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;

import com.schiar.formaisLib.algorithm.GrammarConverter;
import com.schiar.formaisLib.algorithm.Minimizer;
import com.schiar.formaisLib.model.FiniteAutomaton;
import com.schiar.formaisLib.model.RegularGrammar;

public class GrammarConverterTest {

	@Test
	public void testCreateGrammar() {
		String grammar = "S -> aA | bB\nA -> aS | a\nB -> bS | b";
		RegularGrammar g = new RegularGrammar(grammar);
		FiniteAutomaton fa = GrammarConverter.grammarToAutomaton(g);
		List<FiniteAutomaton> automata = Minimizer.minimize(fa);
		FiniteAutomaton mfa = automata.get(automata.size()-1);
		mfa = mfa.complete();
		assertFalse(mfa.isNdfa());
	}
	
	@Test
	public void testCreateAutomatonGrammar() {
		String grammar = "S -> aA | bB\nA -> aS | a\nB -> bS | b";
		RegularGrammar g = new RegularGrammar(grammar);
		FiniteAutomaton fa = GrammarConverter.grammarToAutomaton(g);
		
		RegularGrammar gr =  GrammarConverter.automatonToGrammar(fa);
		FiniteAutomaton fagr = GrammarConverter.grammarToAutomaton(gr);
		assertTrue(fagr.equals(fa));
	}
	
	@Test
	public void testCreateGrammarAutomaton() {
	    String[][] table = {new String[] { "e", "0", "1" },
			 		new String[] { "*->q0", "q0", "q1"},
			 		new String[] { "q1", "q2", "q3" },
			 		new String[] { "q2", "q4", "q0" },
			 		new String[] { "q3", "q1", "q2" },
			 		new String[] { "q4", "q3", "q4" }};
		FiniteAutomaton fa = new FiniteAutomaton("fa1", "binario divisivel por 5", false, table);
		RegularGrammar gr =  GrammarConverter.automatonToGrammar(fa);
		FiniteAutomaton fagr = GrammarConverter.grammarToAutomaton(gr);
		
		assertTrue(fagr.equals(fa));
	}
	
	@Test
	public void testCreateAutomatonGrammar1() {
		String grammar = "S -> aA | &\nA -> aS | a\nB -> bS | b";
		RegularGrammar g = new RegularGrammar(grammar);
		FiniteAutomaton fa = GrammarConverter.grammarToAutomaton(g);
		
		RegularGrammar gr =  GrammarConverter.automatonToGrammar(fa);
		FiniteAutomaton fagr = GrammarConverter.grammarToAutomaton(gr);
		assertTrue(fagr.equals(fa));
	}
}
