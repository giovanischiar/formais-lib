package com.schiar.formaisLib;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.schiar.formaisLib.algorithm.RegexOperations;
import com.schiar.formaisLib.model.regex.Regex;

public class RegexToPolishNotationTest {

	@Test
	public void testRegexToPolishNotation0() {
		Regex regex = new Regex("a|b*");
		String polishNotation = RegexOperations.convertRegexToPolishNotation(regex).getExpression();
		assertEquals("|a*b", polishNotation);
	}
	
	@Test
	public void testRegexToPolishNotation1() {
		Regex regex = new Regex("(a|b)*c?");
		String polishNotation = RegexOperations.convertRegexToPolishNotation(regex).getExpression();
		assertEquals(".*|ab?c", polishNotation);
	}
	
	@Test
	public void testRegexToPolishNotation2() {
		Regex regex = new Regex("a*(b?c|d)*");
		String polishNotation = RegexOperations.convertRegexToPolishNotation(regex).getExpression();
		assertEquals(".*a*|.?bcd", polishNotation);
	}
	
	@Test
	public void testRegexToPolishNotation3() {
		Regex regex = new Regex("(a|b)(a|b)");
		String polishNotation = RegexOperations.convertRegexToPolishNotation(regex).getExpression();
		assertEquals(".|ab|ab", polishNotation);
	}
	
	@Test
	public void testRegexToPolishNotation4() {
		Regex regex = new Regex("((a|b)(a|b))*(a|b)");
		String polishNotation = RegexOperations.convertRegexToPolishNotation(regex).getExpression();
		assertEquals(".*.|ab|ab|ab", polishNotation);
	}
	
	@Test
	public void testRegexToPolishNotation5() {
		Regex regex = new Regex("b*(ab+)a?"); //b*(abb*)a?
		String polishNotation = RegexOperations.convertRegexToPolishNotation(regex).getExpression();
		assertEquals("..*b..ab*b?a", polishNotation);
	}
	
	@Test
	public void testRegexToPolishNotation6() {
		Regex regex = new Regex("b?(ab?ab?)*"); 
		String polishNotation = RegexOperations.convertRegexToPolishNotation(regex).getExpression();
		assertEquals(".?b*...a?ba?b", polishNotation);
	}
	
	@Test
	public void testRegexToPolishNotation7() {
		Regex regex = new Regex("(aa)*(ab)?(bb)*"); 
		String polishNotation = RegexOperations.convertRegexToPolishNotation(regex).getExpression();
		assertEquals("..*.aa?.ab*.bb", polishNotation);
	}

	@Test
	public void testRegexToPolishNotation8() {
		Regex regex = new Regex("(aaa)*(abb|aab)?(bbb)*"); 
		String polishNotation = RegexOperations.convertRegexToPolishNotation(regex).getExpression();
		assertEquals("..*..aaa?|..abb..aab*..bbb", polishNotation);
	}
	
}
