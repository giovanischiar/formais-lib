package com.schiar.formaisLib;

import static org.junit.Assert.*;

import org.junit.Test;

import com.schiar.formaisLib.model.regex.RegexTree;

public class RegexTreeTest {

	@Test
	public void testRegexTree0() {
		RegexTree tree = new RegexTree("|a*b");
		assertTrue(tree.isComplete());
		assertEquals("|a*b", tree.getPolishNotationRegex());
	}
	
	@Test
	public void testRegexTree1() {
		RegexTree tree = new RegexTree(".*|ab?c");
		assertTrue(tree.isComplete());
		assertEquals(".*|ab?c", tree.getPolishNotationRegex());
	}
	
	@Test
	public void testRegexTree2() {
		RegexTree tree = new RegexTree(".*a*|.?bcd");
		assertTrue(tree.isComplete());
		assertEquals(".*a*|.?bcd", tree.getPolishNotationRegex());
	}
	
	@Test
	public void testRegexTree3() {
		RegexTree tree = new RegexTree(".|ab|ab");
		assertTrue(tree.isComplete());
		assertEquals(".|ab|ab", tree.getPolishNotationRegex());
	}
	
	@Test
	public void testRegexTree4() {
		RegexTree tree = new RegexTree(".*.|ab|ab|ab");
		assertTrue(tree.isComplete());
		assertEquals(".*.|ab|ab|ab", tree.getPolishNotationRegex());
	}
	
	@Test
	public void testRegexTree5() {
		RegexTree tree = new RegexTree(".*b..a.b*b?a");
		assertTrue(tree.isComplete());
		assertEquals(".*b..a.b*b?a", tree.getPolishNotationRegex());
	}
	
	@Test
	public void testRegexTree6() {
		RegexTree tree = new RegexTree(".?b*..a?b.a?b");
		assertTrue(tree.isComplete());
		assertEquals(".?b*..a?b.a?b", tree.getPolishNotationRegex());
	}
	
	@Test
	public void testRegexTree7() {
		RegexTree tree = new RegexTree("..*.aa?.ab*.bb");
		assertTrue(tree.isComplete());
		assertEquals("..*.aa?.ab*.bb", tree.getPolishNotationRegex());
	}

	@Test
	public void testRegexTree8() {
		RegexTree tree = new RegexTree("..*.a.aa?|.a.bb.a.ab*.b.bb");
		assertTrue(tree.isComplete());
		assertEquals("..*.a.aa?|.a.bb.a.ab*.b.bb", tree.getPolishNotationRegex());
	}

}
