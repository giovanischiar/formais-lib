package com.schiar.formaisLib;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.junit.Test;

import com.schiar.formaisLib.algorithm.Determinizator;
import com.schiar.formaisLib.algorithm.Minimizer;
import com.schiar.formaisLib.algorithm.SetOperations;
import com.schiar.formaisLib.model.FiniteAutomaton;
import com.schiar.formaisLib.model.State;
import com.schiar.formaisLib.model.State.Type;
import com.schiar.formaisLib.model.StateSet;
import com.schiar.formaisLib.util.SentencesGenerator;

public class FiniteAutomatonToolsTest {

	@Test
	public void testGenerateOppositeAutomaton() {
		String[][] table = {new String[] {"e", "a", "b"},
		        			new String[] {"*->q0", "q1", "q2"},
		        			new String[] {"*q1", "-", "q2"},
		        			new String[] {"*q2", "q1", "-"}};
		FiniteAutomaton fa = new FiniteAutomaton("teste", "sem a's ou b's consecutivos", false, table);
		FiniteAutomaton oppositeFa = SetOperations.generateOppositeAutomaton(fa);
		SentencesGenerator generator = new SentencesGenerator(fa.getAlphabet());
		int n = 3;
		List<String> wordsThatFAAccepted = fa.allSentencesFrom0ToNThatFARecognize(n);
		List<String> wordsThatOppositeFAAccepted = oppositeFa.allSentencesFrom0ToNThatFARecognize(n);
		Set<String> allWordsFrom0ToN = new HashSet<String>(generator.allSentencesFrom0ToN(n));
		for(String word : wordsThatFAAccepted) {
			assertFalse(wordsThatOppositeFAAccepted.contains(word));
		}
		
		List<String> newList = new LinkedList<String>(wordsThatFAAccepted);
		newList.addAll(wordsThatOppositeFAAccepted);
		Set<String> newSet = new HashSet<String>(newList);
		assertEquals(allWordsFrom0ToN, newSet);
	}
	
	@Test
	public void testDeterminize() {
		String[][] table = {new String[] {"e", "a", "b"},
    			new String[] {"->q0", "q1,q0", "q0"},
    			new String[] {"q1", "q2", "-"},
    			new String[] {"*q2", "-", "-"}};
		FiniteAutomaton fa = new FiniteAutomaton("teste", "termina com 'aa'", true, table);
		FiniteAutomaton determinizedFa = Determinizator.determinize(fa);
		int n = 3;
		List<String> wordsThatFAAccepted = fa.allSentencesFrom0ToNThatFARecognize(n);
		List<String> wordsThatDeterminizedFAAccepted = determinizedFa.allSentencesFrom0ToNThatFARecognize(n);
		assertEquals(wordsThatFAAccepted.size(), wordsThatDeterminizedFAAccepted.size());
		for(String word : wordsThatFAAccepted) {
			assertTrue(wordsThatDeterminizedFAAccepted.contains(word));
		}
	}
	
	@Test
	public void testUnion() {
		String[][] table = {new String[] {"e", "a", "b"},
    			new String[] {"->q0", "q1", "q1"},
    			new String[] {"*q1", "q0", "q0"}};
		FiniteAutomaton fa = new FiniteAutomaton("teste", "�mpares", false, table);
		String[][] table2 = {new String[] {"e", "a", "b"},
    						 new String[] {"*->q0", "q1", "q1"},
    						 new String[] {"q1", "q0", "q0"}};
		FiniteAutomaton fa2 = new FiniteAutomaton("teste2", "pares", false, table2);
		SentencesGenerator generator = new SentencesGenerator(fa.getAlphabet());
		int n = 5;
		FiniteAutomaton united = SetOperations.unionOfTwoAutomata(fa, fa2);
		assertEquals(fa.getAlphabet(), united.getAlphabet());
		assertEquals(5, united.getStates().size());
		assertEquals(3, united.getFinalStates().size());
		assertEquals(Type.ACCEPT_INITIAL, united.getInitialState().getType());
		List<String> wordsThatDeterminizedFAAccepted = united.allSentencesFrom0ToNThatFARecognize(n);
		Set<String> allWordsFrom0ToN = new HashSet<String>(generator.allSentencesFrom0ToN(n));
		for(String word : allWordsFrom0ToN) {
			assertTrue(wordsThatDeterminizedFAAccepted.contains(word));
		}
	}
	
	@Test
	public void testIntersection() {
		String[][] table = {new String[] {"e", "a"},
    			new String[] {"->q0", "q1"},
    			new String[] {"*q1", "q0"}};
		FiniteAutomaton fa = new FiniteAutomaton("teste", "impares a", false, table);
		String[][] table2 = {new String[] {"e", "b"},
    						 new String[] {"->q2", "q3"},
    						 new String[] {"*q3", "q2"}};
		FiniteAutomaton fa2 = new FiniteAutomaton("teste2", "pares b", false, table2);
		int n = 5;
		List<FiniteAutomaton> intersectionAutomata = SetOperations.intersectionOfTwoAutomata(fa, fa2);
		FiniteAutomaton intersection = intersectionAutomata.get(intersectionAutomata.size()-1);
		List<String> wordsThatDeterminizedFAAccepted = intersection.allSentencesFrom0ToNThatFARecognize(n);
		assertTrue(wordsThatDeterminizedFAAccepted.isEmpty());
	}
	
	@Test
	public void testReachableStates() {
		String[][] table = { new String[] { "e", "a", "b" },
							 new String[] { "->q0", "q4", "q3" },
							 new String[] { "*q6", "q6", "q6" },
							 new String[] { "q2", "q5", "q5" },
							 new String[] { "*q7", "q7", "q0" },
							 new String[] { "q3", "q0", "q0" },
							 new String[] { "*q5", "q0", "q4" },
							 new String[] { "*q4", "q0", "q3" }};
		FiniteAutomaton fa = new FiniteAutomaton("teste", "palavras de tamanho �mpar", false, table);
		StateSet reachableStates = SetOperations.findReachableStates(fa);
		assertTrue(!reachableStates.contains(new State("q2")));
		assertTrue(!reachableStates.contains(new State("q7")));
		assertTrue(!reachableStates.contains(new State("q8")));
	}
	
	@Test
	public void testIntersectionMinimized() {
		String[][] table1 = {new String[] {"e", "a"},
				 new String[] {"*->A", "A"}};
		FiniteAutomaton fa1 = new FiniteAutomaton("teste", "qualquer coisa", false, table1);
		List<FiniteAutomaton> fa2Automata = SetOperations.intersectionOfTwoAutomata(fa1, fa1);
		FiniteAutomaton fa2 = fa2Automata.get(fa2Automata.size()-1);
		List<FiniteAutomaton> fa3Automata = Minimizer.minimize(fa2);
		FiniteAutomaton fa3 = fa3Automata.get(fa3Automata.size()-1);
		List<FiniteAutomaton> intersectionAutomata = SetOperations.intersectionOfTwoAutomata(fa1, fa3);
		FiniteAutomaton intersection = intersectionAutomata.get(intersectionAutomata.size()-1);
		assertTrue(intersection != null);
	}
	
}
