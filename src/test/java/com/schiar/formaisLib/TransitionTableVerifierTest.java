package com.schiar.formaisLib;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import com.schiar.formaisLib.util.TransitionTableVerifier;

public class TransitionTableVerifierTest {
	
	@Test
	public void testTableWithEmptyTable() {
		String[][] emptyTable = {new String[] {"", "", "", ""},
						    	 new String[] {"", "", "", ""}};
		TransitionTableVerifier t = new TransitionTableVerifier(emptyTable, false);
		List<com.schiar.formaisLib.util.Error> errors = t.getErrors();
		assertTrue(errors.size() == 3);
		assertTrue(errors.contains(com.schiar.formaisLib.util.Error.EMPTYENTRY));
		assertTrue(errors.contains(com.schiar.formaisLib.util.Error.REPEATALPHABET));
		assertTrue(errors.contains(com.schiar.formaisLib.util.Error.WITHOUTINITIALSTATE));
	}

	@Test
	public void testTableWithoutInitialState() {
		String[][] tableWithoutInitialState = {new String[] {"e", "a", "b", "c"},
						    				   new String[] {"q0", "q1", "q1", "q1"},
						    				   new String[] {"*q1", "q0", "q0", "q0"}};
		TransitionTableVerifier t = new TransitionTableVerifier(tableWithoutInitialState, false);
		List<com.schiar.formaisLib.util.Error> errors = t.getErrors();
		assertTrue(errors.size() == 1);
		assertTrue(errors.contains(com.schiar.formaisLib.util.Error.WITHOUTINITIALSTATE));
	}
	
	@Test
	public void testTableWithUndefinedState() {
		String[][] tableWithUndefinedState = {new String[] {"e", "a", "b", "c"},
						    				  new String[] {"->q0", "q1", "q4", "q1"},
						    				  new String[] {"*q1", "q0", "q0", "q0"}};
		TransitionTableVerifier t = new TransitionTableVerifier(tableWithUndefinedState, false);
		List<com.schiar.formaisLib.util.Error> errors = t.getErrors();
		assertTrue(errors.size() == 1);
		assertTrue(errors.contains(com.schiar.formaisLib.util.Error.UNDEFINEDSTATES));
	}
	
	@Test
	public void testTableWithRepeatedStates() {
		String[][] tableWithRepeatedStates = {new String[] {"e", "a", "b", "c"},
						    				  new String[] {"->q0", "q1", "q1", "q1"},
						    				  new String[] {"*q0", "q0", "q0", "q0"},
						    				  new String[] {"q1", "q0", "q1", "q0"}};
		TransitionTableVerifier t = new TransitionTableVerifier(tableWithRepeatedStates, false);
		List<com.schiar.formaisLib.util.Error> errors = t.getErrors();
		assertTrue(errors.size() == 1);
		assertTrue(errors.contains(com.schiar.formaisLib.util.Error.REPEATSTATES));
	}
	
	@Test
	public void testTableWithAllErrors() {
		String[][] tableWithAllErrors = {new String[] {"e", "a", "b", "b"},
						    		     new String[] {"q0", "q1", "q4", "q1"},
						    			 new String[] {"*q0", "q0", "q0", ""}};
		TransitionTableVerifier t = new TransitionTableVerifier(tableWithAllErrors, false);
		List<com.schiar.formaisLib.util.Error> errors = t.getErrors();
		assertTrue(errors.size() == 5);
		assertTrue(errors.contains(com.schiar.formaisLib.util.Error.REPEATSTATES));
		assertTrue(errors.contains(com.schiar.formaisLib.util.Error.UNDEFINEDSTATES));
		assertTrue(errors.contains(com.schiar.formaisLib.util.Error.WITHOUTINITIALSTATE));
		assertTrue(errors.contains(com.schiar.formaisLib.util.Error.EMPTYENTRY));
		assertTrue(errors.contains(com.schiar.formaisLib.util.Error.REPEATALPHABET));
	}

}
