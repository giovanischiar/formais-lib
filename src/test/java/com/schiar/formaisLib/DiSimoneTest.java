package com.schiar.formaisLib;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;

import com.schiar.formaisLib.algorithm.DiSimone;
import com.schiar.formaisLib.algorithm.Minimizer;
import com.schiar.formaisLib.algorithm.SetOperations;
import com.schiar.formaisLib.model.FiniteAutomaton;
import com.schiar.formaisLib.model.regex.Regex;

public class DiSimoneTest {

	@Test
	public void testDiSimone0() {
		Regex regex = new Regex("a|b*");
		FiniteAutomaton automaton = DiSimone.buildAutomaton(regex);
		assertTrue(automaton.reconizeWord(""));
		assertTrue(automaton.reconizeWord("bbbbbbbbb"));
		assertTrue(automaton.reconizeWord("a"));
		assertTrue(automaton.reconizeWord("b"));
		assertTrue(automaton.reconizeWord(""));
		assertFalse(automaton.reconizeWord("ab"));
		assertFalse(automaton.reconizeWord("aa"));
		assertFalse(automaton.reconizeWord("bc"));
	}
	
	@Test
	public void testDiSimone1() {
		Regex regex = new Regex("(a|b)*c?");
		FiniteAutomaton automaton = DiSimone.buildAutomaton(regex);
		assertTrue(automaton.reconizeWord("abbababababababababbababababac"));
		assertTrue(automaton.reconizeWord(""));
		assertTrue(automaton.reconizeWord("aaaaaaaa"));
		assertTrue(automaton.reconizeWord("bbbbbb"));
		assertTrue(automaton.reconizeWord("a"));
		assertTrue(automaton.reconizeWord("b"));
		assertTrue(automaton.reconizeWord("ac"));
		assertTrue(automaton.reconizeWord("bc"));
		assertTrue(automaton.reconizeWord("abc"));
		assertFalse(automaton.reconizeWord("cc"));
		assertFalse(automaton.reconizeWord("cab"));
		assertFalse(automaton.reconizeWord("ababaabcabababa"));
	}
	
	@Test
	public void testDiSimone2() {
		Regex regex = new Regex("a*(b?c|d)*");
		FiniteAutomaton automaton = DiSimone.buildAutomaton(regex);
		assertTrue(automaton.reconizeWord("aaaaaaabcdcdcdcdcdcdbc"));
		assertTrue(automaton.reconizeWord(""));
		assertTrue(automaton.reconizeWord("a"));
		assertTrue(automaton.reconizeWord("bc"));
		assertTrue(automaton.reconizeWord("c"));
		assertTrue(automaton.reconizeWord("d"));
		assertFalse(automaton.reconizeWord("abd"));
		assertFalse(automaton.reconizeWord("adb"));
		assertFalse(automaton.reconizeWord("aabbccdd"));
	}
	
	@Test
	public void testDiSimone3() {
		Regex regex = new Regex("(a|b)(a|b)");
		FiniteAutomaton automaton = DiSimone.buildAutomaton(regex);
		assertTrue(automaton.reconizeWord("ab"));
		assertTrue(automaton.reconizeWord("aa"));
		assertTrue(automaton.reconizeWord("bb"));
		assertTrue(automaton.reconizeWord("ba"));
		assertFalse(automaton.reconizeWord(""));
		assertFalse(automaton.reconizeWord("aab"));
		assertFalse(automaton.reconizeWord("a"));
	}
	
	@Test
	public void testDiSimone4() {
		Regex regex = new Regex("((a|b)(a|b))*(a|b)");
		FiniteAutomaton automaton = DiSimone.buildAutomaton(regex);
		assertTrue(automaton.reconizeWord("abb"));
		assertTrue(automaton.reconizeWord("babababababaa"));
		assertTrue(automaton.reconizeWord("b"));
		assertTrue(automaton.reconizeWord("bbbbb"));
		assertFalse(automaton.reconizeWord(""));
		assertFalse(automaton.reconizeWord("aa"));
		assertFalse(automaton.reconizeWord("abbb"));
	}
	
	@Test
	public void testDiSimone5() {
		Regex regex = new Regex("b*(ab+)a?");
		FiniteAutomaton automaton = DiSimone.buildAutomaton(regex);
		assertTrue(automaton.reconizeWord("bbbbabbbbbbbbbbbbbba"));
		assertTrue(automaton.reconizeWord("bbab"));
		assertTrue(automaton.reconizeWord("abbbbb"));
		assertTrue(automaton.reconizeWord("ab"));
		assertFalse(automaton.reconizeWord(""));
		assertFalse(automaton.reconizeWord("aa"));
		assertFalse(automaton.reconizeWord("b"));
	}
	
	@Test
	public void testDiSimone6() {
		Regex regex = new Regex("b?(ab?ab?)*");
		FiniteAutomaton automaton = DiSimone.buildAutomaton(regex);
		assertTrue(automaton.reconizeWord("babababababab"));
		assertTrue(automaton.reconizeWord(""));
		assertTrue(automaton.reconizeWord("b"));
		assertTrue(automaton.reconizeWord("aa"));
		assertTrue(automaton.reconizeWord("aaaa"));
		assertTrue(automaton.reconizeWord("abaaa"));
		assertFalse(automaton.reconizeWord("bb"));
		assertFalse(automaton.reconizeWord("aaa"));
		assertFalse(automaton.reconizeWord("babaa"));
	}
	
	@Test
	public void testDiSimone7() {
		Regex regex = new Regex("(aa)*(ab)?(bb)*");
		FiniteAutomaton automaton = DiSimone.buildAutomaton(regex);
		assertTrue(automaton.reconizeWord("aaaaaaabbbbbbb"));
		assertTrue(automaton.reconizeWord(""));
		assertTrue(automaton.reconizeWord("aabb"));
		assertTrue(automaton.reconizeWord("aaabbb"));
		assertTrue(automaton.reconizeWord("ab"));
		assertTrue(automaton.reconizeWord("aaab"));
		assertTrue(automaton.reconizeWord("abbbbb"));
		assertFalse(automaton.reconizeWord("a"));
		assertFalse(automaton.reconizeWord("b"));
		assertFalse(automaton.reconizeWord("ba"));
	}
	
	@Test
	public void testDiSimone8() {
		Regex regex = new Regex("(aaa)*(abb|aab)?(bbb)*");
		FiniteAutomaton automaton = DiSimone.buildAutomaton(regex);
		assertTrue(automaton.reconizeWord("aaaaaaabbbbbbbb"));
		assertTrue(automaton.reconizeWord(""));
		assertTrue(automaton.reconizeWord("aaabbb"));
		assertTrue(automaton.reconizeWord("aaaaabbbb"));
		assertTrue(automaton.reconizeWord("aab"));
		assertTrue(automaton.reconizeWord("abbbbb"));
		assertTrue(automaton.reconizeWord("abb"));
		assertFalse(automaton.reconizeWord("aaabb"));
		assertFalse(automaton.reconizeWord("a"));
		assertFalse(automaton.reconizeWord("ba"));
	}
	
	@Test
	public void testDiSimone9() {
		Regex regex = new Regex("(a((abc)*(def)*)?)*");
		FiniteAutomaton automaton = DiSimone.buildAutomaton(regex);
		assertTrue(automaton.reconizeWord("aabcabcdefdefdef"));
		assertTrue(automaton.reconizeWord(""));
		assertTrue(automaton.reconizeWord("a"));
		assertTrue(automaton.reconizeWord("aabcdefdefaabc"));
		assertTrue(automaton.reconizeWord("aabc"));
		assertTrue(automaton.reconizeWord("adef"));
		assertTrue(automaton.reconizeWord("aaaaaaaaaaaaa"));
		assertTrue(automaton.reconizeWord("adefa"));
		assertTrue(automaton.reconizeWord("aaaaabc"));
		assertFalse(automaton.reconizeWord("defa"));
		assertFalse(automaton.reconizeWord("aabcdefdefabc"));
		assertFalse(automaton.reconizeWord("abc"));
		assertFalse(automaton.reconizeWord("abcab"));
		assertFalse(automaton.reconizeWord("abcde"));
		assertFalse(automaton.reconizeWord("b"));
		assertFalse(automaton.reconizeWord("aabb"));
	}
	
	@Test
	public void testDiSimone10() {
		Regex regex = new Regex("(ba|a(ba)*a)*(ab)*");
		FiniteAutomaton automaton = DiSimone.buildAutomaton(regex);
		assertTrue(automaton.reconizeWord(""));
		assertFalse(automaton.reconizeWord("a"));
		assertTrue(automaton.reconizeWord("aa"));
		assertFalse(automaton.reconizeWord("aba"));
		assertTrue(automaton.reconizeWord("baab"));
		assertFalse(automaton.reconizeWord("babab"));
		assertFalse(automaton.reconizeWord("bababb"));
		assertFalse(automaton.reconizeWord("abbab"));
	}
	
	@Test
	public void testDiSimone11() {
		Regex regexA = new Regex("(aa|bb|ab|ba)*");
		Regex regexB = new Regex("(abcd)*");
		FiniteAutomaton faA = DiSimone.buildAutomaton(regexA);
		FiniteAutomaton faB = DiSimone.buildAutomaton(regexB);
		FiniteAutomaton faAUB = SetOperations.unionOfTwoAutomata(faA, faB);
		Regex regexU = new Regex("((aa|bb|ab|ba)*)|((abcd)*)");
		FiniteAutomaton faU = DiSimone.buildAutomaton(regexU);
		assertTrue(faAUB.equals(faU));
	}
	
	@Test
	public void testDiSimone12() {
		Regex regex = new Regex("(((a|b)(a|b))*)|(((a|b)(a|b))*(a|b))");
		Regex regexA = new Regex("((a|b)(a|b))*");
		Regex regexB = new Regex("((a|b)(a|b))*(a|b)");
		FiniteAutomaton faA = DiSimone.buildAutomaton(regexA);
		FiniteAutomaton faB = DiSimone.buildAutomaton(regexB);
		FiniteAutomaton faAUB = SetOperations.unionOfTwoAutomata(faA, faB);
		FiniteAutomaton automaton = DiSimone.buildAutomaton(regex);
		assertTrue(faAUB.equals(automaton));
		assertTrue(automaton.reconizeWord(""));
		assertTrue(automaton.reconizeWord("a"));
		assertTrue(automaton.reconizeWord("b"));
		assertTrue(automaton.reconizeWord("aa"));
		assertTrue(automaton.reconizeWord("ab"));
		assertTrue(automaton.reconizeWord("ba"));
		assertTrue(automaton.reconizeWord("bb"));
		assertTrue(automaton.reconizeWord("aaa"));
		assertTrue(automaton.reconizeWord("aab"));
		assertTrue(automaton.reconizeWord("aba"));

	}
	
	@Test
	public void testDiSimone13() {
		Regex regex = new Regex("(a|b)a*b");
		FiniteAutomaton automaton = DiSimone.buildAutomaton(regex);
		assertTrue(automaton.reconizeWord("ab"));
		assertTrue(automaton.reconizeWord("bb"));
		assertTrue(automaton.reconizeWord("aab"));
		assertTrue(automaton.reconizeWord("bab"));
		assertTrue(automaton.reconizeWord("aaab"));
		assertTrue(automaton.reconizeWord("baab"));
		assertTrue(automaton.reconizeWord("aaaab"));
		assertTrue(automaton.reconizeWord("baaab"));
		assertTrue(automaton.reconizeWord("aaaaab"));
		assertTrue(automaton.reconizeWord("baaaab"));
	}
	
	
	@Test
	public void testDiSimone14() {
		Regex regex = new Regex("((a?|b*(a*|b))(b*|a))|(a((a|b*)(b|a))*)");
		FiniteAutomaton automaton = DiSimone.buildAutomaton(regex);
		Regex regexA = new Regex("(a?|b*(a*|b))(b*|a)");
		Regex regexB = new Regex("a((a|b*)(b|a))*");
		FiniteAutomaton faA = DiSimone.buildAutomaton(regexA);
		FiniteAutomaton faB = DiSimone.buildAutomaton(regexB);
		FiniteAutomaton faAUB = SetOperations.unionOfTwoAutomata(faA, faB);
		FiniteAutomaton x = Minimizer.minimizeAndReturnLastResultingAutomaton(faAUB);
		FiniteAutomaton y = Minimizer.minimizeAndReturnLastResultingAutomaton(automaton);
		System.out.println(x);
		System.out.println(y);
		assertTrue(faAUB.equals(automaton));
		assertTrue(automaton.reconizeWord(""));
		assertTrue(automaton.reconizeWord("a"));
		assertTrue(automaton.reconizeWord("b"));
		assertTrue(automaton.reconizeWord("aa"));
		assertTrue(automaton.reconizeWord("ab"));
		assertTrue(automaton.reconizeWord("ba"));
		assertTrue(automaton.reconizeWord("bb"));
		assertTrue(automaton.reconizeWord("aaa"));
		assertTrue(automaton.reconizeWord("aab"));
		assertTrue(automaton.reconizeWord("aba"));
		assertTrue(automaton.reconizeWord("abb"));
		assertTrue(automaton.reconizeWord("baa"));
		assertTrue(automaton.reconizeWord("bab"));
		assertTrue(automaton.reconizeWord("bba"));
		assertTrue(automaton.reconizeWord("bbb"));
		assertTrue(automaton.reconizeWord("aabababababbababababaa"));
		assertFalse(automaton.reconizeWord("baba"));

	}
}
