package com.schiar.formaisLib.algorithm;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.schiar.formaisLib.model.Alphabet;
import com.schiar.formaisLib.model.FiniteAutomaton;
import com.schiar.formaisLib.model.regex.Regex;
import com.schiar.formaisLib.model.regex.RegexElement;
import com.schiar.formaisLib.model.regex.RegexTree;
import com.schiar.formaisLib.util.Node;

public class DiSimone {

	private class IntermediateState {
		public String name;
		public Map<Character, String> transitions;
		public Set<RegexElement> compositions;
		public List<RegexElement> sourceCompositions; 
		
		public IntermediateState(String name, Map<Character, String> transitions, Set<RegexElement> compositions, List<RegexElement> sourceCompositions) {
			this.name = name;
			this.transitions = transitions;
			this.compositions = compositions;
			this.sourceCompositions = sourceCompositions;
		}
	}
	
	private static RegexElement LAMBDA_ELEMENT = new RegexElement('&');
	
	private List<IntermediateState> intermediateStates = new ArrayList<>();
	private RegexTree tree;
	private boolean goingDown;
	private Alphabet newAlphabet;
	private int lastUsedIndex;
	private RegexElement startingElement;
	private IntermediateState currentState;
	private boolean firstRun;
	private boolean canceledRun;
	private List<RegexElement> pendingElements;
	
	public static FiniteAutomaton buildAutomaton(Regex regex) {
		DiSimone diSimone = new DiSimone();
		FiniteAutomaton automaton = diSimone.runDiSimone(regex);
		return automaton;
	}
	
	private FiniteAutomaton runDiSimone(Regex regex) {
		Regex polishNotationRegex = RegexOperations.convertRegexToPolishNotation(regex);
		tree = new RegexTree(polishNotationRegex);
		List<Character> operands = polishNotationRegex.getListOfOperands();
		Collections.sort(operands);
		newAlphabet = new Alphabet(operands);
		
		lastUsedIndex = 0;
		String name = "q" + lastUsedIndex++;
		IntermediateState firstState = new IntermediateState(name, new HashMap<Character, String>(), new HashSet<RegexElement>(), new ArrayList<RegexElement>());
		intermediateStates.add(firstState);

		firstRun = true;
		runDiSimoneIteration();
		
		String[][] table = createAutomatonTable();
		FiniteAutomaton automaton = new FiniteAutomaton(regex.getName() + "(from regex)", regex.getDescription() + "(from regex)", false, table);
		return automaton;
	}

	private void runDiSimoneIteration() {
		if (!areThereUnresolvedStates()) {
			return;
		}

		pendingElements = new ArrayList<>();
		
		if (firstRun) {
			canceledRun = false;
			startingElement = null; //only used when starting from a leaf node
			goingDown = true;
			currentState = intermediateStates.get(0);
			runThroughRegexTree(tree.getRoot());
			if (resolveCurrentState()) {
				createNewIntermediateStates();
			}
			firstRun = false;
		} else {
			List<IntermediateState> statesToIterate = new ArrayList<>(intermediateStates);
			for (IntermediateState state : statesToIterate) {
				if (!state.compositions.isEmpty()) {
					continue;
				}
				
				currentState = state;

				boolean ranThroughTree = false;
				for (Character character : newAlphabet) {
					for (RegexElement element : state.sourceCompositions) {
						if (!element.getElement().equals(character)) {
							continue;
						}

						canceledRun = false;
						runThroughRegexTree(element);
						ranThroughTree = true;
					}
				}
				
				if (ranThroughTree) {
					if (resolveCurrentState()) {
						createNewIntermediateStates();
					} else {
						intermediateStates.remove(currentState);
					}
				}
			}
		}
		
		runDiSimoneIteration();
	}
	
	private void runThroughRegexTree(RegexElement element) {
		startingElement = element;
		goingDown = false;
		runThroughRegexTree(tree.getRoot());
	}
	
	private void runThroughRegexTree(Node<RegexElement> node) {
		int foundStartingElementInBranch = -1; //only used when starting from a leaf node
		if (startingElement != null) {
			if (node.getElement().equals(startingElement)) {
				startingElement = null;
				return;
			} else {
				for (int i = 0; i < node.getChildCount(); i++) {
					runThroughRegexTree(node.getChildAt(i));
					
					if (startingElement == null) {
						if (canceledRun) {
							return;
						}
						foundStartingElementInBranch = i;
						break;
					}
				}
				
				if (startingElement != null) {
					return;
				}
			}
		}
		
		RegexElement element = node.getElement();
		
		if (element.isOperand()) {
			handleNewOperand(node);
			return;
		}
		
		loop: for (int i = 0; i < node.getChildCount(); i++) {
			Node<RegexElement> child = node.getChildAt(i);
			switch (element.getElement()) {
			case '*':
				goingDown = true;
				pendingElements.add(element);
				runThroughRegexTree(child);
				pendingElements.remove(element);
				goingDown = false;
				break;
			case '?':
				if (goingDown) {
					pendingElements.add(element);
					runThroughRegexTree(child);
					pendingElements.remove(element);
					goingDown = false;
				}
				break;
			case '.':
				if (goingDown) {
					runThroughRegexTree(child);
					if (goingDown) {
						break loop;
					}
				} else if (i > 0 && foundStartingElementInBranch <= 0) {
					goingDown = true;
					runThroughRegexTree(child);
					if (goingDown && !child.getElement().isUnaryOperator() && pendingElements.isEmpty()) {
						canceledRun = true;
					}
				}
				break;
			case '|':
				if (goingDown) {
					if (i == 0) {
						pendingElements.add(element);
					}
					runThroughRegexTree(child);
					if (goingDown && i == 0) {
						pendingElements.remove(element);
					}
				} else if (i > 0 && pendingElements.contains(element)) {
					goingDown = true;
					runThroughRegexTree(child);					
					pendingElements.remove(element);
					goingDown = false;

				}
				break;
			}
		}
		
		if (tree.getRoot().getElement().equals(node.getElement())) {
			if (!goingDown && !canceledRun) {
				handleLambdaOperand();
			}
		}
	}

	private void handleNewOperand(Node<RegexElement> node) {
		RegexElement element = node.getElement();
		currentState.compositions.add(element);
	}
	
	private void handleLambdaOperand() {
		currentState.compositions.add(LAMBDA_ELEMENT);
	}
	
	private boolean resolveCurrentState() {
		boolean foundEquivalentState = false;
		for (IntermediateState equivalentState : intermediateStates) {
			if (currentState.equals(equivalentState)) {
				continue;
			}
			
			if (currentState.compositions.equals(equivalentState.compositions)) {
				for (IntermediateState state : intermediateStates) {
					if (currentState.equals(state)) {
						continue;
					}
					
					for (Map.Entry<Character, String> entry : state.transitions.entrySet()) {
						if (entry.getValue().equals(currentState.name)) {
							entry.setValue(equivalentState.name);
						}
					}
				}
				foundEquivalentState = true;
				break;
			}
		}
		
		if (foundEquivalentState) {
			return false;
		}
		
		for (RegexElement element : currentState.compositions) {
			Character elementChar = element.getElement();
			if (!elementChar.equals(LAMBDA_ELEMENT.getElement()) && !currentState.transitions.containsKey(elementChar)) {
				currentState.transitions.put(elementChar, "q" + lastUsedIndex++);
			}
		}
		return true;
	}
	
	private void createNewIntermediateStates() {
		List<IntermediateState> statesToAdd = new ArrayList<>();
		for (IntermediateState state : intermediateStates) {
			for (Map.Entry<Character, String> entry : state.transitions.entrySet()) {
				if (!isStateCreated(entry.getValue())) {
					List<RegexElement> source = getCompositionsWith(state, entry.getKey());
					IntermediateState newState = new IntermediateState(entry.getValue(), new HashMap<Character, String>(), new HashSet<RegexElement>(), source);
					statesToAdd.add(newState);
				}
			}
		}
		for (IntermediateState newState : statesToAdd) {
			intermediateStates.add(newState);
		}
	}
	
	private List<RegexElement> getCompositionsWith(IntermediateState state, Character character) {
		List<RegexElement> sourceCompositions = new ArrayList<>();
		for (RegexElement element : state.compositions) {
			if (element.getElement().equals(character)) {
				sourceCompositions.add(element);
			}
		}
		return sourceCompositions;
	}

	private boolean isStateCreated(String stateName) {
		for (IntermediateState state : intermediateStates) {
			if (state.name.equals(stateName)) {
				return true;
			}
		}
		return false;
	}
	
	private boolean areThereUnresolvedStates() {
		for (IntermediateState state : intermediateStates) {
			if (state.compositions.isEmpty()) {
				return true;
			}
		}
		return false;
	}
	
	private String[][] createAutomatonTable() {
		String[][] table = new String[intermediateStates.size()+1][newAlphabet.size()+1];
    	table[0][0] = "δ";

    	for (int i = 1; i < table.length; i++) {
    		IntermediateState state = intermediateStates.get(i-1);
    		StringBuilder stateName = new StringBuilder();
    		if (state.compositions.contains(LAMBDA_ELEMENT)) {
    			stateName.append('*');
    		}
    		if (i-1 == 0) {
    			stateName.append("->");
    		}
    		stateName.append(state.name);
    		table[i][0] = stateName.toString();
    	}
    	
    	for (int j = 1; j < table[0].length; j++) {
    		table[0][j] = String.valueOf(newAlphabet.get(j-1));
    	}
    	
    	for (int i = 1; i < table.length; i++) {
    		for (int j = 1; j < table[0].length; j++) {
        		IntermediateState state = intermediateStates.get(i-1);
        		Character character = table[0][j].charAt(0);
        		if (state.transitions.containsKey(character)) {
        			table[i][j] = state.transitions.get(character);
        		} else {
        			table[i][j] = FiniteAutomaton.TRAPSTATE;
        		}
        	}
    	}
    	
    	return table;
	}
	
}
