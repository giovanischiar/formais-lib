package com.schiar.formaisLib.algorithm;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import com.schiar.formaisLib.model.regex.Regex;

public class RegexOperations {

	public static Regex convertRegexToPolishNotation(Regex regex) {
		String expression = regex.getCanonicalExpression();
		List<Character> list = makePolishNotationList(expression);
		StringBuilder polishNotation = new StringBuilder();
		for (Character character : list) {
			polishNotation.append(character);
		}
		String polishNotationExpression = polishNotation.toString();
		Regex newRegex = new Regex(regex.getName(), regex.getDescription(), polishNotationExpression);
		return newRegex;
	}
	
	public static List<Character> makePolishNotationList(String regex) {
		List<Character> list = new ArrayList<>();
		Stack<Character> stack = new Stack<>();
		
		for (int i = regex.length()-1; i >= 0; i--) {
			char character = regex.charAt(i);
			switch (character) {
			case '(':
				while (!stack.peek().equals(')')) {
					list.add(stack.pop());
				}
				stack.pop();
				break;
			case ')':
				stack.push(character);
				break;
			case '|':
				while (!stack.isEmpty() && (stack.peek().equals('.') || stack.peek().equals('*') || stack.peek().equals('?'))) {
					list.add(stack.pop());
				}
				stack.push(character);
				break;
			case '.':
				while (!stack.isEmpty() && (stack.peek().equals('*') || stack.peek().equals('?'))) {
					list.add(stack.pop());
				}
				stack.push(character);
				break;
			case '*':
			case '?':
				stack.push(character);
				break;
			default:
				list.add(character);
			}
		}
		
		while (!stack.isEmpty()) {
			list.add(stack.pop());
		}
		
		List<Character> reversed = new ArrayList<>(list.size());
		for (int i = list.size()-1; i >= 0; i--) {
			reversed.add(list.get(i));
		}
		
		return reversed;
	}
	
}
