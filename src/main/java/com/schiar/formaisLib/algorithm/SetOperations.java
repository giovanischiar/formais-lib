package com.schiar.formaisLib.algorithm;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.schiar.formaisLib.model.Alphabet;
import com.schiar.formaisLib.model.FiniteAutomaton;
import com.schiar.formaisLib.model.Pair;
import com.schiar.formaisLib.model.State;
import com.schiar.formaisLib.model.State.Type;
import com.schiar.formaisLib.model.StateList;
import com.schiar.formaisLib.model.StateSet;
import com.schiar.formaisLib.model.TransitionMap;

public class SetOperations {
	
	public static FiniteAutomaton generateOppositeAutomaton(FiniteAutomaton fa) {
		StateList states = fa.getStates().generateOppositeStates();
		TransitionMap newTransitions = fa.getTransitions().copyUsingNewStates(new StateSet(states));
		return new FiniteAutomaton("\u00AC(" + fa.getName() + ")", "\u00AC(" + fa.getDesc() + ")", fa.isNdfa(), states, fa.getAlphabet().copy(), newTransitions);
	}
	
	public static FiniteAutomaton unionOfTwoAutomata(FiniteAutomaton fa1, FiniteAutomaton fa2) {
		FiniteAutomaton fa2StatesRenamed = renameIfRepeatedState(fa1, fa2);
		Type typeOfNewInitialState = Type.INITIAL;
		TransitionMap newTransitions = new TransitionMap();
		StateSet initialStates = new StateSet();
		initialStates.add(fa1.getInitialState());
		initialStates.add(fa2StatesRenamed.getInitialState());
		for(State initialState : initialStates) {
			if(initialState.isAccept()) {
				typeOfNewInitialState = Type.ACCEPT_INITIAL;
				break;
			}
		}
		State newInitialState = new State(initialStates.joinStateNames(), typeOfNewInitialState);
		
		Alphabet unionOfTwoAlphabets = Alphabet.unionOfAlphabets(fa1.getAlphabet(), fa2StatesRenamed.getAlphabet());
		StateSet unionOfTwoStates = new StateSet(fa1.getStates()).union(new StateSet(fa2StatesRenamed.getStates()));
		unionOfTwoStates = unionOfTwoStates.changeInitialStatesToNotInitial();
		unionOfTwoStates.add(newInitialState);
		addTransitionsForNewInitialState(fa1, newTransitions, newInitialState);
		addTransitionsForNewInitialState(fa2StatesRenamed, newTransitions, newInitialState);
		newTransitions.putAll(fa1.getTransitions().copyUsingNewStates(unionOfTwoStates));
		newTransitions.putAll(fa2StatesRenamed.getTransitions().copyUsingNewStates(unionOfTwoStates));
		FiniteAutomaton result = new FiniteAutomaton(fa1.getName() + " \u222a " + fa2.getName(), fa1.getDesc() + " \u222a " + fa2.getDesc(), true, new StateList(unionOfTwoStates), unionOfTwoAlphabets, newTransitions);
		FiniteAutomaton automatonWithRenamedStates = result.copyWithNewStateNames();
		FiniteAutomaton completedAutomaton = automatonWithRenamedStates.complete();
		return completedAutomaton.copyWithAllStatesRenamed();
	}
	
	public static FiniteAutomaton renameIfRepeatedState(FiniteAutomaton fa1, FiniteAutomaton fa2) {
		StateSet statesCandidates = new StateSet();
		TransitionMap newTransitions = fa2.getTransitions();
		for(State q : fa2.getStates()) {
			if(!fa1.getStates().contains(q)) {
				statesCandidates.add(q);
			} else {
				State stateRenamed = new State(q.getName() + "2", q.getType());
				statesCandidates.add(stateRenamed);
				newTransitions = renameOldTransitionsToNewState(q, stateRenamed, newTransitions);
			}
		}
		return new FiniteAutomaton(fa2.getName(), fa2.getDesc(), fa2.isNdfa(), new StateList(statesCandidates), fa2.getAlphabet(), newTransitions);
	}
	
	public static TransitionMap renameOldTransitionsToNewState(State q, State stateRenamed, final TransitionMap transition) {
		TransitionMap newTransitions = new TransitionMap();
		for(Map.Entry<Pair<State, Character>, StateSet> entry : transition.entrySet()) {
			Pair<State, Character> newKey = new Pair<>(entry.getKey());
			StateSet newValue = new StateSet();
			for(State state : entry.getValue()) {
				newValue.add(new State(state));
			}
			
			if (entry.getKey().first().equals(q)) {
				newKey = new Pair<State, Character>(stateRenamed, entry.getKey().second());
			}
			
			if (entry.getValue().contains(q)) {
				StateSet newStates = new StateSet();
				for(State state : entry.getValue()) {
					newStates.add(state.equals(q) ? stateRenamed : state);
				}
				newValue = newStates;
			}
			newTransitions.put(newKey, newValue);
		}
		return newTransitions;
		
	}
	
	public static void addTransitionsForNewInitialState(FiniteAutomaton fa, TransitionMap newTransitions, State newInitialState) {
		for(Map.Entry<Pair<State, Character>, StateSet> entry : fa.getTransitions().entrySet()) {
			if (entry.getKey().first().equals(fa.getInitialState())) {
				Pair<State, Character> key = new Pair<State, Character>(newInitialState, entry.getKey().second());
				if(!newTransitions.containsKey(key)) {
					newTransitions.put(key, entry.getValue());
				} else {
					newTransitions.put(key, newTransitions.get(key).union(entry.getValue()));
				}
			}
		}
	}	
	
	public static StateSet findReachableStates(FiniteAutomaton fa) {
		StateSet reachableStates = new StateSet();
		reachableStates.add(fa.getInitialState());
		return findReachableStates(fa, reachableStates, fa.getInitialState());
	}
	
	public static StateSet findReachableStates(final FiniteAutomaton fa, StateSet reachableStates, State currentState) {		
		for(Character letter : fa.getAlphabet()) {
			Pair<State, Character> newKey = new Pair<>(currentState, letter);
			Set<State> states = new HashSet<>(fa.getTransitions().get(newKey));
			Set<State> newStates = new HashSet<>();
			for(State newState : states) {
				if(!reachableStates.contains(newState)) {
					newStates.add(newState);
				}
			}
			reachableStates.addAll(states);
			for(State state : newStates) {
				findReachableStates(fa, reachableStates, state);
			}
		}
		return new StateSet(reachableStates);
	}
	
	public static boolean isFiniteAutomatonProduceNoSentence(FiniteAutomaton fa) {
		StateSet states = findReachableStates(fa);
		for(State state : states) {
			if(state.isAccept()) {
				return false;
			}
		}
		return true;
	}
	
	public static List<FiniteAutomaton> intersectionOfTwoAutomata(FiniteAutomaton fa1, FiniteAutomaton fa2) {
		List<FiniteAutomaton> resultAutomata = new ArrayList<>();
		
		Alphabet unionOfTwoAlphabets = Alphabet.unionOfAlphabets(fa1.getAlphabet(), fa2.getAlphabet());
		FiniteAutomaton newFA1 = new FiniteAutomaton(fa1.getName(), fa1.getDesc(), fa1.isNdfa(), fa1.getStates().copy(), unionOfTwoAlphabets, fa1.getTransitions().copy()); 
		FiniteAutomaton newFA2 = new FiniteAutomaton(fa2.getName(), fa2.getDesc(), fa2.isNdfa(), fa2.getStates().copy(), unionOfTwoAlphabets, fa2.getTransitions().copy()); 
		FiniteAutomaton completeFA1 = newFA1.complete();
		FiniteAutomaton completeFA2 = newFA2.complete();
		FiniteAutomaton oppositeFa1 = SetOperations.generateOppositeAutomaton(completeFA1);
		resultAutomata.add(oppositeFa1);
		FiniteAutomaton oppositeFa2 = SetOperations.generateOppositeAutomaton(completeFA2);
		resultAutomata.add(oppositeFa2);
		FiniteAutomaton unionOfOpposites = SetOperations.unionOfTwoAutomata(oppositeFa1, oppositeFa2);
		resultAutomata.add(unionOfOpposites);
		FiniteAutomaton determinizedUnion = Determinizator.determinize(unionOfOpposites);
		resultAutomata.add(determinizedUnion);
		FiniteAutomaton intersectionAutomaton = SetOperations.generateOppositeAutomaton(determinizedUnion);
		intersectionAutomaton.setName(fa1.getName() + " \u2229 " + fa2.getName());
		intersectionAutomaton.setDesc(fa1.getDesc() + " \u2229 " + fa2.getDesc());
		resultAutomata.add(intersectionAutomaton.copyWithAllStatesRenamed());
		return resultAutomata;
	}
}
