package com.schiar.formaisLib.algorithm;

import java.util.List;

import com.schiar.formaisLib.model.Alphabet;
import com.schiar.formaisLib.model.FiniteAutomaton;
import com.schiar.formaisLib.model.Pair;
import com.schiar.formaisLib.model.RegularGrammar;
import com.schiar.formaisLib.model.State;
import com.schiar.formaisLib.model.State.Type;
import com.schiar.formaisLib.model.StateList;
import com.schiar.formaisLib.model.StateSet;
import com.schiar.formaisLib.model.TransitionMap;

public class GrammarConverter {
	public static FiniteAutomaton grammarToAutomaton(RegularGrammar g) {
		StateList statelist = new StateList();
		Alphabet alphabet = new Alphabet();
		TransitionMap transitions = new TransitionMap();
		State finalState = new State("final", Type.ACCEPT);
		statelist.add(finalState);
		for(Character vn : g.getNonTerminals()) {
			Type type = Type.REGULAR;
			if(g.getInitialState() == vn) {
				type = Type.INITIAL;
				if(!g.getAllProductionsOf(vn, '&').isEmpty()) {
					type = Type.ACCEPT_INITIAL;
				};
			}
			State s = new State(vn.toString(), type);
			statelist.add(s);
		}
		
		for(Character vt : g.getTerminals()) {
			alphabet.add(vt);
			State empty = new State("-");
			StateSet emptySet = new StateSet();
			emptySet.add(empty);
			transitions.put(new Pair<State, Character>(finalState, vt), emptySet);
		}
		
		boolean isNdfa = false;
		
		for(Character vn : g.getNonTerminals()) {
			State s = new State(vn.toString());
			
			for(Character vt : g.getTerminals()) {
				List<Pair<String, Pair<Character, Character>>> pairProductions = g.getAllProductionsOf(vn, vt);
				Pair<State, Character> pair = new Pair<>(s, vt); 
				StateSet stateSet = new StateSet();
				for(Pair<String, Pair<Character, Character>> production : pairProductions) {
					if(production.second().second() == '&') {
						stateSet.add(finalState);
						continue;
					}
					stateSet.add(new State(production.second().second().toString()));
				}
				if(!stateSet.isEmpty()) {
					if(stateSet.size() > 1) {
						isNdfa = true;
					}
					transitions.put(pair, stateSet);
				}
				
			}
		}
		return new FiniteAutomaton(g.getName() + " (from grammar)", g.getDescription(), isNdfa, statelist, alphabet, transitions).complete().copyWithAllStatesRenamed();
	}
	
	private static String addProduction(String production, String rightSide) {
		if(!rightSide.isEmpty()) {
			rightSide += " | ";
		}
		return rightSide += production;
	}
	
	public static RegularGrammar automatonToGrammar(FiniteAutomaton automatonStateStyle) {
		FiniteAutomaton fa = automatonStateStyle.copyWithGrammarStatesStyle();
		String grammar = "";
		for(State vn : fa.getStates()) {

			
			if(vn.getName() == "-") {
				continue;
			}
			String leftSide =  vn.getName() + " -> "; 
			String rightSide = "";
			
			if(vn.getType() == Type.ACCEPT_INITIAL) {
				rightSide = addProduction("&", rightSide);
			}
			
			for(Character a : fa.getAlphabet()) {
				Pair<State, Character> p = new Pair<>(vn, a);
				StateSet stateSet = fa.getTransitions().get(p);
				if(stateSet == null) continue;
				for(State s : stateSet) {
					String stateName = s.getName();
					if(s.isAccept()) {
						rightSide = addProduction(a.toString(), rightSide);
					}
										
					if(stateName != "-") {
						rightSide = addProduction(a + stateName, rightSide);
					}
				}
			}
			if(!rightSide.isEmpty()) {
				grammar += leftSide + rightSide + "\n";			
			}
		}
		
//		
//		for(int i = 0; i < fa.getStates().size(); i++) {
//			
//			for(Character)
//			for(fa.getTransitions().get(key))
//			
//		}
//		
		return new RegularGrammar(fa.getName() + " (from automaton)", fa.getDesc(), grammar);
	}
}
