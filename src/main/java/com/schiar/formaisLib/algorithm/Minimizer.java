package com.schiar.formaisLib.algorithm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.schiar.formaisLib.model.FiniteAutomaton;
import com.schiar.formaisLib.model.Pair;
import com.schiar.formaisLib.model.State;
import com.schiar.formaisLib.model.StateList;
import com.schiar.formaisLib.model.StateSet;

public class Minimizer {
	
	public static FiniteAutomaton minimizeAndReturnLastResultingAutomaton(FiniteAutomaton automaton) {
		List<FiniteAutomaton> fas = minimize(automaton);
		return fas.get(fas.size()-1);
	}

	public static List<FiniteAutomaton> minimize(FiniteAutomaton automaton) {
		List<FiniteAutomaton> resultAutomata = new ArrayList<>();
		
		automaton = automaton.complete();
		
		if (automaton.isNdfa()) {
			automaton = Determinizator.determinize(automaton);
			resultAutomata.add(automaton);
		}
		
		automaton = removeUnreachableStates(automaton);
		String newAutomatonName = automaton.getName() + " (no unreachable states)";
		automaton.setName(newAutomatonName);		
		String newAutomatonDescription = "no-unreachable-states version of " + automaton.getDesc(); 
		automaton.setDesc(newAutomatonDescription);
		resultAutomata.add(automaton);
		
		automaton = removeDeadStates(automaton);
		automaton = automaton.complete();
		newAutomatonName = automaton.getName() + " (no dead states)";
		automaton.setName(newAutomatonName);		
		newAutomatonDescription = "no-dead-states version of " + automaton.getDesc(); 
		automaton.setDesc(newAutomatonDescription);
		resultAutomata.add(automaton);
		
		Map<String, StateSet> equivalenceClasses = new HashMap<>();		
		StateSet finalStates = automaton.getFinalStates();
		StateSet nonFinalStates = new StateSet();
		for (State state : automaton.getStates()) {
			if (!finalStates.contains(state)) {
				nonFinalStates.add(state);
			}
		}
		equivalenceClasses.put("0", nonFinalStates);
		equivalenceClasses.put("1", finalStates);
		
		equivalenceClasses = generateEquivalenceClasses(automaton, equivalenceClasses);
		Set<StateSet> equivalenceClassesSet = new HashSet<>(equivalenceClasses.values());
		
		automaton = automaton.joinEquivalenceClasses(equivalenceClassesSet);
		automaton = removeDeadStates(automaton);
		FiniteAutomaton minimized = new FiniteAutomaton(automaton.getName() + " (minimized)", "minimized version of " + automaton.getDesc(), false, automaton.getStates(), automaton.getAlphabet(), automaton.getTransitions());
		resultAutomata.add(minimized.copyWithAllStatesRenamed());
		
		return resultAutomata;
	}
	
	public static FiniteAutomaton removeUnreachableStates(FiniteAutomaton automaton) {
		automaton = automaton.copy();
		StateSet reachableStates = SetOperations.findReachableStates(automaton);
		StateSet unreachableStates = new StateSet();
		for (State state : automaton.getStates()) {
			if (!reachableStates.contains(state)) {
				unreachableStates.add(state);
			}
		}
		
		FiniteAutomaton noUnreachableStates = automaton.removeStates(unreachableStates);		
		return noUnreachableStates;
	}
	
	public static FiniteAutomaton removeDeadStates(FiniteAutomaton automaton) {
		automaton = automaton.copy();
		StateSet deadStates = findDeadStates(automaton);
		FiniteAutomaton noDeadStates = automaton.removeStates(deadStates);
		return noDeadStates;
	}
	
	public static StateSet findDeadStates(FiniteAutomaton automaton) {
		StateSet deadStateCandidates = new StateSet();
		for (State state : automaton.getStates()) {
			if (!automaton.getFinalStates().contains(state)) {
				deadStateCandidates.add(state);
			}
		}
		
		StateSet deadStates = new StateSet();
		for (State deadStateCandidate : deadStateCandidates) {
			StateSet reachableStates = SetOperations.findReachableStates(automaton, new StateSet(), deadStateCandidate);
			boolean isDead = true;
			for (State state : reachableStates) {
				if (automaton.getFinalStates().contains(state)) {
					isDead = false;
					break;
				}
			}
			
			if (isDead) {
				deadStates.add(deadStateCandidate);
			}
		}
		
		return deadStates;
	}
	
	private static Map<String, StateSet> generateEquivalenceClasses(FiniteAutomaton automaton, Map<String, StateSet> currentClasses) {
		Map<String, StateSet> newClasses = new HashMap<>();
		for (Map.Entry<String, StateSet> entry : currentClasses.entrySet()) {
			Map<String, StateSet> localNewClasses = new HashMap<>();
			for (State state : entry.getValue()) {
				StateList transitionsFromState = getTransitionsFrom(state, automaton);
				String newClassId = calculateClassIdFor(transitionsFromState, currentClasses);
				if (localNewClasses.containsKey(newClassId)) {
					localNewClasses.get(newClassId).add(state);
				} else {
					StateSet newSet = new StateSet();
					newSet.add(state);
					localNewClasses.put(newClassId, newSet);
				}
			}
			for (StateSet set : localNewClasses.values()) {
				newClasses.put(String.valueOf(newClasses.size()), set);
			}
		}
		
		Set<StateSet> newClassesSet = new HashSet<>(newClasses.values());
		Set<StateSet> currentClassesSet = new HashSet<>(currentClasses.values());
		if (newClassesSet.equals(currentClassesSet)) {
			return newClasses;
		}
		
		return generateEquivalenceClasses(automaton, newClasses);
	}
	
	public static StateList getTransitionsFrom(State state, FiniteAutomaton automaton) {
		Map<Integer, State> stateTransitions = new HashMap<>();
		for (Map.Entry<Pair<State, Character>, StateSet> entry : automaton.getTransitions().entrySet()) {
			if (entry.getKey().first().equals(state)) {
				if (entry.getValue().size() != 1) {
					throw new RuntimeException("Transition from " + state.getName() + " with " + entry.getKey().second() + " is not unitary.");
				}
				stateTransitions.put(automaton.getAlphabet().indexOf(entry.getKey().second()), entry.getValue().first());
			}
		}
		if (automaton.getAlphabet().size() != stateTransitions.size()) {
			throw new RuntimeException("There are not transitions with all symbols in the alphabet.");
		}
		StateList states = new StateList(automaton.getAlphabet().size());
		for (int i = 0; i < automaton.getAlphabet().size(); i++) {
			if (stateTransitions.containsKey(i)) {
				states.add(stateTransitions.get(i));
			} else {
				throw new RuntimeException("No transition from " + state.getName() + " with " + automaton.getAlphabet().get(i) + ".");
			}
		}
		return states;
	}
	
	public static String calculateClassIdFor(StateList states, Map<String, StateSet> currentClasses) {
		StringBuilder classId = new StringBuilder();
		for (State state : states) {
			boolean found = false;
			for (Map.Entry<String, StateSet> entry : currentClasses.entrySet()) {
				if (entry.getValue().contains(state)) {
					classId.append(entry.getKey());
					found = true;
					break;
				}
			}
			if (!found) {
				throw new RuntimeException("Couldn't find an equivalence class with state " + state.getName() + ".");
			}
		}
		
		return classId.toString();
	}
	
}
