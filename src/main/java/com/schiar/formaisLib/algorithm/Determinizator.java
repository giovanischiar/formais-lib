package com.schiar.formaisLib.algorithm;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.schiar.formaisLib.model.Alphabet;
import com.schiar.formaisLib.model.FiniteAutomaton;
import com.schiar.formaisLib.model.Pair;
import com.schiar.formaisLib.model.State;
import com.schiar.formaisLib.model.StateList;
import com.schiar.formaisLib.model.StateSet;
import com.schiar.formaisLib.model.TransitionMap;

public class Determinizator {
	
    public static FiniteAutomaton determinize(FiniteAutomaton fa) {
    	FiniteAutomaton automaton = fa.copy();
		StateList statesFromFA = automaton.getStates();
		StateSet finalStates = automaton.getFinalStates();
		Alphabet alphabet = automaton.getAlphabet();
		TransitionMap newTransitions = automaton.getTransitions();
		while (isThereUndeterminedStates(newTransitions, statesFromFA)) {
			List<StateSet> undeterminedStates = retrieveStatesOfKeysWithUndeterminacy(newTransitions);
			for(int i = 0; i < undeterminedStates.size(); i++) {
				for(char letter : alphabet) {
					StateSet states = undeterminedStates.get(i);
					State newPossiblyState = states.joinStates();
					newTransitions.put(new Pair<State, Character>(newPossiblyState, letter), reachedStates(states, letter, newTransitions));
					if(!statesFromFA.contains(newPossiblyState)) {
						statesFromFA.add(newPossiblyState);
					}
					if (newPossiblyState.isAccept() && !finalStates.contains(newPossiblyState)) {
						finalStates.add(newPossiblyState);
					}
				}
			}
		}
		joinUndeterminadedStates(newTransitions);
		FiniteAutomaton result = new FiniteAutomaton(automaton.getName() + " (determinized)", "determinized version of " + automaton.getDesc(), false, statesFromFA, alphabet, newTransitions);
		FiniteAutomaton automatonWithRenamedStates = result.copyWithNewStateNames();
		return automatonWithRenamedStates.copyWithAllStatesRenamed();
	}
    
    public static void joinUndeterminadedStates(TransitionMap transitions) {
    	for(Map.Entry<Pair<State, Character>, StateSet> entry : transitions.entrySet()) {
    		if(entry.getValue().size() > 1) {
    			StateSet uniqueState = new StateSet();
    			uniqueState.add(entry.getValue().joinStates());
    			entry.setValue(uniqueState);
    		}
    	}
    }
	
	public static boolean isThereUndeterminedStates(TransitionMap transitions, StateList states) {
		for(Pair<State, Character> key : transitions.keySet()) {
			if(transitions.get(key).size() > 1) {
				State state = transitions.get(key).joinStates();
				if(!states.contains(state)) {
					return true;
				}
			}
		}
		return false;
	}
		
	public static StateSet reachedStates(StateSet states, Character letter, TransitionMap transitions) {
		StateSet reachedStates = new StateSet();
		for(State state : states) {
			Pair<State, Character> key = new Pair<State, Character>(state, letter);
			StateSet newReachedStates = transitions.get(key);
			for(State newReachedState : newReachedStates)  {
				if (!reachedStates.contains(newReachedState)) {
					reachedStates.add(newReachedState);
				}
			}
			State trapState = new State(FiniteAutomaton.TRAPSTATE);
			if (newReachedStates.contains(trapState)) {
				reachedStates.remove(trapState);
			}
		}
		return reachedStates;
	}
	
	public static List<StateSet> retrieveStatesOfKeysWithUndeterminacy(TransitionMap transitions) {
		List<StateSet> undeterminedStates = new ArrayList<>();
		for(Map.Entry<Pair<State, Character>, StateSet> entry : transitions.entrySet()) {
			StateSet resultStates = entry.getValue();
			if(resultStates.size() > 1) {
				undeterminedStates.add(resultStates);
			}
		}
		return undeterminedStates;
	}
	
}
