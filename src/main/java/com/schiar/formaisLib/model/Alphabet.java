package com.schiar.formaisLib.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class Alphabet extends ArrayList<Character> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2998859673566020205L;

	public Alphabet() {
		super();
	}
	
	public Alphabet(int capacity) {
		super(capacity);
	}
	
    public Alphabet(Collection<Character> collection) {
    	super(collection);
    }
	
	public Alphabet copy() {
		Alphabet copy = new Alphabet(size());
		for (Character character : this) {
			copy.add(new Character(character));
		}
		return copy;
	}
	
	public static Alphabet unionOfAlphabets(Alphabet alphabet1, Alphabet alphabet2) {
		Set<Character> set1 = new HashSet<>(alphabet1);
		Set<Character> set2 = new HashSet<>(alphabet2);
		set1.addAll(set2);
		Alphabet newAlphabet = new Alphabet();
		newAlphabet.addAll(set1);
		return newAlphabet;
	}
	
}
