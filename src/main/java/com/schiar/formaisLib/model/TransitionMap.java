package com.schiar.formaisLib.model;

import java.util.HashMap;
import java.util.Map;

public class TransitionMap extends HashMap<Pair<State, Character>, StateSet> {

    /**
	 * 
	 */
	private static final long serialVersionUID = -74433666301589411L;

	public TransitionMap() {
        super();
    }
	
    public TransitionMap(int initialCapacity) {
        super(initialCapacity);
    }
    
    public TransitionMap copy() {
    	TransitionMap newTransitions = new TransitionMap(size());
		for (Map.Entry<Pair<State, Character>, StateSet> entry : entrySet()) {
			Pair<State, Character> newKey = new Pair<>(new State(entry.getKey().first()), new Character(entry.getKey().second()));
			StateSet newValue = entry.getValue().copy();
			newTransitions.put(newKey, newValue);
		}
		return newTransitions;
    }
    
    public TransitionMap copyUsingNewStates(StateSet states) {
    	TransitionMap newTransitions = new TransitionMap(size());
		for (Map.Entry<Pair<State, Character>, StateSet> entry : entrySet()) {
			Pair<State, Character> newKey = new Pair<>(states.findStateByName(entry.getKey().first().getName()), new Character(entry.getKey().second()));
			StateSet newValue = new StateSet(entry.getValue().size());
			for (State oldState : entry.getValue()) {
				newValue.add(states.findStateByName(oldState.getName()));
			}
			newTransitions.put(newKey, newValue);
		}
		return newTransitions;
    }
	
}
