package com.schiar.formaisLib.model;

import java.util.ArrayList;
import java.util.List;

public class RegularGrammar {
	private String name;
	private String description;
	private Character initialState;
	private List<Character> terminals;
	private List<Character> nonTerminals;
	private List<Pair<String, Pair<Character, Character>>> productionRules;
	private String grammarText;
	
	private RegularGrammar(String name, String description) {
		this.name = name;
		this.description = description;
	}
	
	public RegularGrammar(String name, String description, List<Character> nonTerminals, List<Character> terminals, List<Pair<String, Pair<Character, Character>>> productionRules, Character initialState) {
		this(name, description);
		this.initialState = initialState;
		this.terminals = terminals;
		this.nonTerminals = nonTerminals;
		this.productionRules = productionRules;
	}
	
	public RegularGrammar(String grammar) {
		this("", "");
		extractSymbols(grammar);
		extractProductions(grammar);
		this.grammarText = grammar;
	}
	
	public RegularGrammar(String name, String description, String grammar) {
		this(name, description);
		extractSymbols(grammar);
		extractProductions(grammar);
		this.grammarText = grammar;
	}
	
	private void extractSymbols(String grammar) {
		nonTerminals = new ArrayList<>();
		terminals = new ArrayList<>();
		for(Character c : grammar.replaceAll("\\s", "").toCharArray()) {
			if(Character.isUpperCase(c) && !nonTerminals.contains(c)) {
				nonTerminals.add(c);
			} else if ((Character.isLowerCase(c) || Character.isDigit(c)) && !terminals.contains(c)) {
				terminals.add(c);
			}
		}
		
		initialState = nonTerminals.get(0);
	}
	
	public void extractProductions(String grammar) {
		productionRules = new ArrayList<>();
		String[] rows = grammar.split("\n");
		for(String row : rows) {
			String[] prod = row.replaceAll("\\s", "").split("->");
			addProduction(prod[0], prod[1]);
		}
	}
	
	public void addProduction(String alpha, String betasPipe) {
		String[] betas = betasPipe.replaceAll("\\s", "").split("\\|");
		for(String beta : betas) {
			Character second = '&';
			if(beta.length() > 1) {
				second = beta.charAt(1);
			}
			Pair<Character, Character> pair = new Pair<>(beta.charAt(0), second); 
			productionRules.add(new Pair<String, Pair<Character, Character>>(alpha, pair));
		}
	}
	
	public List<Pair<String, Pair<Character, Character>>> getAllProductionsOfPair(Pair<Character, Character> pair) {
		List<Pair<String, Pair<Character, Character>>> p = new ArrayList<>();
		for(Pair<String, Pair<Character, Character>> production: productionRules) {
			if(production.second().equals(pair)) {
				p.add(production);
			}
		}
		return p;
	}
	
	public List<Pair<String, Pair<Character, Character>>> getAllProductionsOf(Character vn, Character vt) {
		List<Pair<String, Pair<Character, Character>>> p = new ArrayList<>();
		for(Pair<String, Pair<Character, Character>> production: productionRules) {
			if(production.first().equals(vn.toString()) && production.second().first() == vt) {
				p.add(production);
			}
		}
		return p;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public Character getInitialState() {
		return initialState;
	}

	public List<Character> getTerminals() {
		return terminals;
	}

	public List<Character> getNonTerminals() {
		return nonTerminals;
	}

	public List<Pair<String, Pair<Character, Character>>> getProductionRules() {
		return productionRules;
	}

	public String getGrammarText() {
		return grammarText;
	}
}
