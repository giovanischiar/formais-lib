package com.schiar.formaisLib.model;

import java.util.Arrays;
import java.util.List;

public class State implements Comparable<State> {
	
	private final String name;
	private final Type type;
	
	public enum Type {INITIAL, ACCEPT, ACCEPT_INITIAL, REGULAR}
	
	public State(String name) {
		this(name, Type.REGULAR);
	}
		
	public State(String name, Type type) {
		this.name = name;
		this.type = type;
	}
	
	public State(State other) {
		this.name = other.name;
		this.type = other.type;
	}

	public String getName() {
		return name;
	}
	
	public Type getType() {
		return type;
	}

	public boolean isInitial() {
		return type == Type.INITIAL || type == Type.ACCEPT_INITIAL;
	}

	public boolean isAccept() {
		return type == Type.ACCEPT || type == Type.ACCEPT_INITIAL;
	}
	
	@Override
	public int hashCode(){
	    return (this.toString()).hashCode();
	}

	@Override
	public boolean equals(Object o){
	    return (this.toString()).equals(o.toString());
	}

	@Override
	public int compareTo(State other) {
		if (this == other) {
			return 0;
		}
		
		if (other == null) {
			return 1;
		}
		
		return name.compareTo(other.name);
	}
	
	public int compareTypes(State other) {
		if(this.isInitial()) {
			return -1;
		} else if(other.isInitial()) {
			return 1;
		} else if(!this.isAccept() && other.isAccept()) {
			return -1;
		}  else if(this.isAccept() && !other.isAccept()) {
			return 1;
		}
		
		return 0;
	}
	
	public String fullToString() {
		String strName = name;
		if(isInitial()) {
			strName = "->".concat(name);
		}
		
		if(isAccept()) {
			strName = "*".concat(strName);
		}
		return strName;
	}
	
	@Override
	public String toString() {
		return name;
	}
	
	public static StateList filterStateListByStringArray(String[] strArrayStates, StateList states) { 
		StateList filteredStates = new StateList();
		List<String> strStates = Arrays.asList(strArrayStates);
		for(State state : states) {
			if(strStates.contains(state.getName())) {
				filteredStates.add(state);
			}
		}
		
		if(filteredStates.isEmpty() && strArrayStates[0].equals(FiniteAutomaton.TRAPSTATE)) {
			filteredStates.add(new State(FiniteAutomaton.TRAPSTATE));
     	}
		return filteredStates;
	}

}
