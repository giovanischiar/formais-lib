package com.schiar.formaisLib.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.schiar.formaisLib.model.State.Type;

public class StateList extends ArrayList<State> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5193529157775251972L;

	public StateList() {
		super();
	}
	
	public StateList(int capacity) {
		super(capacity);
	}
	
	public StateList(Collection<? extends State> stateCollection) {
		super(stateCollection);
	}
	
	public StateList copy() {
		StateList copy = new StateList(size());
		for (State state : this) {
			copy.add(new State(state));
		}
		return copy;
	}
	
	public State findStateByName(String name) {
		for(State state : this) {
			if(state.getName().equals(name)) {
				return state;
			}
		}
		return null;
	}
	
	public List<State> findStatesByType(Type type) {
		List<State> states = new ArrayList<State>();
		for(State state : this) {
			if(state.getType() == type) {
				states.add(state);
			}
		}
		return states;
	}
	
	public StateList generateOppositeStates() {
		StateList newList = new StateList();
		for (State state : this) {
			Type newType = Type.REGULAR; 
			if (state.isAccept()) {
				if (state.isInitial()) {
					newType = Type.INITIAL;
				}
			} else {
				if (state.isInitial()) {
					newType = Type.ACCEPT_INITIAL;
				} else {
					newType = Type.ACCEPT;
				}
			}
			newList.add(new State(state.getName(), newType));
		}
		return newList;
	}
	
	public StateList getReachableStates(State init, StateList states, TransitionMap transitions, Alphabet alphabet) {
		if(this.size() == states.size()) {
			return states;
		}
		for(Character c : alphabet) {
			StateSet stateSet = transitions.get(new Pair<State, Character>(init, c));
			if(stateSet != null) {
				for(State state : stateSet) {
					if(!states.contains(state)) {
						states.add(state);
						getReachableStates(state, states, transitions, alphabet);
					}
				}

			}
		}
		return states;
	}
	
	public StateList toOrderedStates(TransitionMap transitions, Alphabet alphabet) {
		StateList states = new StateList();
		states.addAll(findStatesByType(Type.INITIAL));
		states.addAll(findStatesByType(Type.ACCEPT_INITIAL));
		states = getReachableStates(states.get(0), states, transitions, alphabet);
		if(states.size() < this.size()) {
			StateList unreachableStates =  new StateList(this);
			unreachableStates.removeAll(states);
			states.addAll(unreachableStates);
		}
		return states;
	}
	
}
