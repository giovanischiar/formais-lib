package com.schiar.formaisLib.model;

import java.util.List;

public class TransitionTable {
	private String[][] table;
	
	public TransitionTable(String[][] table) {
		this.table = table;
	}
	
	public String[][] getTable() {
		return table;
	}
	
    public TransitionTable(StateList states, TransitionMap transitions, Alphabet alphabet) {
    	//List<State> states = statesSet.toOrderedStates(transitions, alphabet);
    	table = new String[states.size()+1][alphabet.size()+1];
    	fillTableWithStates(table, states);
    	fillTableWithAlphabet(table, alphabet);
    	table[0][0] = "\u1E9F";
    	for(int i = 1; i < table.length; i++) {
    		for(int j = 1; j < table[0].length; j++) {
    			String stateName = new String(table[i][0]);
           	 	if(stateName.startsWith("*")) {
           	 		stateName = stateName.replaceAll("\\*", "");
           	 	} 
           	 	if(stateName.startsWith("->")) {
           	 		stateName = stateName.replaceAll("->", "");
           	 	}
    			Pair<State, Character> transitionKey = new Pair<>(states.findStateByName(stateName), alphabet.get(j-1));
    			if (!transitions.containsKey(transitionKey)) {
        			table[i][j] = FiniteAutomaton.TRAPSTATE;
        			continue;
    			}
    			
        		if(transitions.get(transitionKey).size() == 1) {
        			table[i][j] = transitions.get(transitionKey).first().getName();
        		} else {
        			table[i][j] = transitions.get(transitionKey).toString();
        		}
        	}
    	}
    }
    
    private void fillTableWithStates(String[][] table, List<State> states) {
    	for(int i = 1; i < table.length; i++) {
    		State state = states.get(i-1);
    		table[i][0] = state.fullToString();
    	}
    }
    
    private void fillTableWithAlphabet(String[][] table, Alphabet alphabet) {
    	for(int i = 1; i < table[0].length; i++) {
    		table[0][i] = String.valueOf(alphabet.get(i-1));
    	}
    }
}
