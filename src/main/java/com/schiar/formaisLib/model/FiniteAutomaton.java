package com.schiar.formaisLib.model;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import com.schiar.formaisLib.algorithm.Determinizator;
import com.schiar.formaisLib.algorithm.Minimizer;
import com.schiar.formaisLib.algorithm.SetOperations;
import com.schiar.formaisLib.model.State.Type;
import com.schiar.formaisLib.util.FASentencesGenerator;

/**
 * Created by Giovani on 17/10/2016.
 */
public class FiniteAutomaton {
	public static final String TRAPSTATE = "-";
	private String id;

	private String name;
	private String desc;
	private boolean ndfa;

	private StateList states;
	private StateSet finalStates;
	private Alphabet alphabet;
	private State initialState;
	private TransitionMap transitions;

	public FiniteAutomaton(String name, String desc, boolean ndfa, StateList states, Alphabet alphabet, TransitionMap transitions) {
		this(name, desc, ndfa);
		this.states = states;
		this.alphabet = alphabet;
		this.transitions = transitions;
		findInitialState();
		buildFinalStates();
	}

	private void findInitialState() {
		for (State state : states) {
			if (state.isInitial()) {
				initialState = state;
				return;
			}
		}
	}

	private void buildFinalStates() {
		finalStates = new StateSet();
		for (State state : states) {
			if (state.isAccept()) {
				finalStates.add(state);
			}
		}
	}

	private FiniteAutomaton(String name, String desc, boolean ndfa) {
		id = UUID.randomUUID().toString();
		this.name = name;
		this.desc = desc;
		this.ndfa = ndfa;
	}

	// constructor
	public FiniteAutomaton(String name, String desc, boolean ndfa, String[][] table) {
		this(name, desc, ndfa);
		build(table);
	}
	
	public FiniteAutomaton(String name, String desc, String[][] table) {
		this(name, desc, false);
		ndfa = isNdfa(table);
		build(table);
	}
	
	private boolean isNdfa(String[][] table) {
		
		return true;
	}

	// methods for build automata

	private void build(String[][] table) {
		buildAlphabet(table);
		buildStates(table);
		buildTransitions(table);
	}

	private void buildAlphabet(String[][] table) {
		String[] firstRow = table[0];
		alphabet = new Alphabet();
		for (int i = 1; i < firstRow.length; i++) {
			alphabet.add(firstRow[i].charAt(0));
		}
	}

	private void buildStates(String[][] table) {
		states = new StateList();
		finalStates = new StateSet();
		for (int i = 1; i < table.length; i++) {
			String rawState = table[i][0];
			if (rawState.startsWith("->")) {
				State state = new State(rawState.replace("->", ""), Type.INITIAL);
				initialState = state;
				states.add(state);
				continue;
			}
			if (rawState.startsWith("*->")) {
				State state = new State(rawState.replace("*->", ""), Type.ACCEPT_INITIAL);
				initialState = state;
				finalStates.add(state);
				states.add(state);
				continue;
			}
			if (rawState.startsWith("*")) {
				State state = new State(rawState.replace("*", ""), Type.ACCEPT);
				finalStates.add(state);
				states.add(state);
				continue;
			}
			states.add(new State(rawState));
		}
	}

	private void buildTransitions(String[][] table) {
		if (transitions != null) return;
		transitions = new TransitionMap();
		for (int i = 1; i < table.length; i++) {
			for (int j = 1; j < table[0].length; j++) {
				StateSet newStates = new StateSet();
				String[] stringStates = table[i][j].split(",");
				if (ndfa) {
					newStates = new StateSet(State.filterStateListByStringArray(stringStates, states));
				} else {
					StateSet newIntermediateStates = new StateSet(State.filterStateListByStringArray(stringStates, states));
					newStates.add(newIntermediateStates.joinStates());
				}
				transitions.put(new Pair<State, Character>(states.get(i - 1), alphabet.get(j - 1)), newStates);
			}
		}
		if (needTrapTransition(table)) {
			addTrapTransition();
		}
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null || !(obj instanceof FiniteAutomaton)) {
			return false;
		}
		
		FiniteAutomaton other = (FiniteAutomaton)obj;
		return this.containsOrEquals(Minimizer.minimizeAndReturnLastResultingAutomaton(other)) && other.containsOrEquals(Minimizer.minimizeAndReturnLastResultingAutomaton(this));
	}

	public boolean containsOrEquals(FiniteAutomaton other) {
		FiniteAutomaton _fa1 = this;
		FiniteAutomaton _fa2 = other;
		if(_fa1.isNdfa()) {
			_fa1 = Determinizator.determinize(_fa1);
    	}
    	if(_fa2.isNdfa()) {
    		_fa2 = Determinizator.determinize(_fa2);
    	}
    	FiniteAutomaton oppositeFa = SetOperations.generateOppositeAutomaton(_fa2);
    	List<FiniteAutomaton> intersectionAutomata = SetOperations.intersectionOfTwoAutomata(_fa1, oppositeFa);
    	FiniteAutomaton intersection = intersectionAutomata.get(intersectionAutomata.size()-1);
    	return SetOperations.isFiniteAutomatonProduceNoSentence(intersection);
	}
	
	public FiniteAutomaton copy() {
		FiniteAutomaton copy = new FiniteAutomaton(name, desc, ndfa, states.copy(), alphabet.copy(), transitions.copy());
		return copy;
	}

	public FiniteAutomaton copyWithNewStateNames() {
		List<Integer> stateNameIndexes = new ArrayList<>(states.size());
		String prefix = "q";
		StateList renamedStates = new StateList();
		StateList newStates = new StateList();
		TransitionMap copiedTransitions = transitions.copy();

		for (int i = 0; i < states.size(); i++) {
			String stateName = prefix + i;
			State stateWithCorrectName = states.findStateByName(stateName);
			if (stateWithCorrectName != null && !renamedStates.contains(stateWithCorrectName)) {
				State newState = new State(stateWithCorrectName);
				newStates.add(newState);
				stateNameIndexes.add(states.indexOf(newState));
				continue;
			}

			State stateToRename = null;
			for (int j = 0; j < states.size(); j++) {
				if (stateNameIndexes.contains(j)) {
					continue;
				}

				stateToRename = states.get(j);
				stateNameIndexes.add(j);
				break;
			}

			if (stateToRename == null) {
				break;
			}

			State newState = new State(stateName, stateToRename.getType());
			newStates.add(newState);
			copiedTransitions = copyTransitionsWithRenamedState(copiedTransitions, stateToRename, newState);
			renamedStates.add(stateToRename);
		}

		FiniteAutomaton newFiniteAutomaton = new FiniteAutomaton(name, desc, ndfa, newStates, alphabet.copy(), copiedTransitions);
		return newFiniteAutomaton;
	}
	
	private TransitionMap copyTransitionsWithRenamedState(TransitionMap oldTransitions, State stateToRename, State stateRenamed) {
		TransitionMap renamingTransitions = new TransitionMap(oldTransitions.size());
		for (Map.Entry<Pair<State, Character>, StateSet> entry : oldTransitions.entrySet()) {
			Pair<State, Character> newKey = entry.getKey();
			if (newKey.first().equals(stateToRename)) {
				newKey = new Pair<>(stateRenamed, newKey.second());
			}

			StateSet newValue = entry.getValue().copy();
			newValue.replace(stateToRename, stateRenamed);

			renamingTransitions.put(newKey, newValue);
		}
		return renamingTransitions;
	}
	
	public FiniteAutomaton renameState(State state, String newName) {
		State newState = new State(newName, state.getType());
		StateList newStates = new StateList(states);
		newStates.set(states.indexOf(state), newState);
		TransitionMap newTransitions = copyTransitionsWithRenamedState(transitions.copy(), state, newState);
		return new FiniteAutomaton(name, desc, ndfa, newStates, alphabet.copy(), newTransitions);
	}
	
	/**
	 * Create new automaton with all states renamed according to order of stateList.
	*/
	public FiniteAutomaton copyWithAllStatesRenamed(String style) {
		int i = 0;
		StateList orderedStates = this.states.toOrderedStates(transitions, alphabet);
		FiniteAutomaton currentAutomaton = new FiniteAutomaton(name, desc, ndfa, orderedStates, alphabet.copy(), transitions.copy());
		for(State state : orderedStates) {
			String stateObfucated = UUID.randomUUID().toString();
			if(state.getName() == "-") {
				stateObfucated = state.getName();
			}
			currentAutomaton = currentAutomaton.renameState(state, stateObfucated);
		}
		
		FiniteAutomaton intermediateAutomaton = currentAutomaton;
		
		for(State state : currentAutomaton.states) {
			String stateName = "q" + i;
			if(style == "gr") {
				stateName = Character.toString ((char) (65 + i));
			}
			if(state.getName() == "-") {
				stateName = state.getName();
			}
			
			i++;
			intermediateAutomaton = intermediateAutomaton.renameState(state, stateName);
		}
		
		return intermediateAutomaton;
	}
	
	public FiniteAutomaton copyWithAllStatesRenamed() {
		return copyWithAllStatesRenamed("af");
	}
	
	public FiniteAutomaton copyWithGrammarStatesStyle() {
		return copyWithAllStatesRenamed("gr");
		
	}

	public FiniteAutomaton removeStates(StateSet states) {
		FiniteAutomaton automaton = copy();
		StateList newStates = new StateList();
		for (State state : automaton.getStates()) {
			if (!states.contains(state)) {
				newStates.add(state);
			}
		}

		TransitionMap newTransitions = new TransitionMap();
		for (Map.Entry<Pair<State, Character>, StateSet> entry : automaton.getTransitions().entrySet()) {
			if (states.contains(entry.getKey().first())) {
				continue;
			}

			StateSet newValue = new StateSet();
			for (State state : entry.getValue()) {
				if (!states.contains(state)) {
					newValue.add(state);
				}
			}

			if (!newValue.isEmpty()) {
				newTransitions.put(entry.getKey(), newValue);
			}
		}
		
		if(newStates.size() == 0) {
			newStates.add(new State("q0", Type.INITIAL));
		}

		FiniteAutomaton newAutomaton = new FiniteAutomaton(automaton.getName(), automaton.getDesc(), automaton.isNdfa(), newStates, automaton.getAlphabet(), newTransitions);
		return newAutomaton;
	}

	public FiniteAutomaton complete() {
		FiniteAutomaton automaton = copy();
		State phiState = new State(TRAPSTATE);
		boolean addedPhiState = false;
		for (State state : automaton.getStates()) {
			for (Character character : automaton.getAlphabet()) {
				Pair<State, Character> key = new Pair<>(state, character);
				if (!automaton.getTransitions().containsKey(key) || automaton.getTransitions().get(key).isEmpty()) {
					StateSet newValue = new StateSet(1);
					newValue.add(phiState);
					automaton.getTransitions().put(key, newValue);
					addedPhiState = true;
				}
			}
		}

		if (addedPhiState) {
			StateSet set = new StateSet();
			set.add(phiState);
			for (Character character : automaton.getAlphabet()) {
				automaton.getTransitions().put(new Pair<>(phiState, character), set);
			}
			automaton.getStates().add(phiState);
		}
		return automaton;
	}

	public FiniteAutomaton joinEquivalenceClasses(Set<StateSet> equivalenceClasses) {
		FiniteAutomaton automaton = copy();
		StateList states = automaton.getStates();
		TransitionMap transitions = automaton.getTransitions();
		for (StateSet equivalenceClass : equivalenceClasses) {
			if (equivalenceClass.size() <= 1) {
				continue;
			}

			StateList stateList = new StateList(equivalenceClass);
			State chosenState;
			if (stateList.contains(automaton.getInitialState())) {
				chosenState = automaton.getInitialState();
			} else {
				chosenState = stateList.get(0);
			}
			TransitionMap newTransitions = new TransitionMap();
			for (Map.Entry<Pair<State, Character>, StateSet> entry : transitions.entrySet()) {
				State state = entry.getKey().first();
				if (stateList.contains(state) && !chosenState.equals(state)) {
					continue;
				}

				StateSet newValue = new StateSet();
				for (State valueState : entry.getValue()) {
					if (stateList.contains(valueState)) {
						newValue.add(chosenState);
					} else {
						newValue.add(valueState);
					}
				}

				newTransitions.put(entry.getKey(), newValue);
			}
			transitions = newTransitions;

			StateList newStates = new StateList();
			for (State state : states) {
				if (!stateList.contains(state) || chosenState.equals(state)) {
					newStates.add(state);
				}
			}
			states = newStates;
		}

		FiniteAutomaton newAutomaton = new FiniteAutomaton(name, desc, ndfa, states, alphabet.copy(), transitions);
		return newAutomaton;
	}

	private boolean needTrapTransition(String[][] table) {
		for (StateSet value : transitions.values()) {
			if (value.isUnitary() && value.first().getName().equals(TRAPSTATE)) {
				for (int i = 0; i < table.length; i++) {
					String[] row = table[i];
					if (allArrayPositionsIsTheSame(row, TRAPSTATE)) {
						return false;
					}
				}
				return true;
			}
		}
		return false;
	}

	private boolean allArrayPositionsIsTheSame(String[] array, String word) {
		for (int i = 0; i < array.length; i++) {
			if (!array[i].equals(word)) {
				return false;
			}
		}
		return true;
	}

	private void addTrapTransition() {
		StateSet trapList = new StateSet();
		State trap = new State(TRAPSTATE);
		trapList.add(trap);
		for (int i = 0; i < alphabet.size(); i++) {
			transitions.put(new Pair<State, Character>(trap, alphabet.get(i)), trapList);
		}
		states.add(trap);
	}

	// getters

	public Alphabet getAlphabet() {
		return alphabet;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public boolean isNdfa() {
		return ndfa;
	}

	public State getInitialState() {
		return initialState;
	}

	public TransitionMap getTransitions() {
		return transitions;
	}

	public StateSet getFinalStates() {
		return finalStates;
	}

	public StateList getStates() {
		return states;
	}

	public String[][] generateArrayRepresentationOfTransitions() {
		return new TransitionTable(states, transitions, alphabet).getTable();
	}
	
	@Override
	public String toString() {
		String repres = "";
		String[][] matrix = generateArrayRepresentationOfTransitions();
		for(int i = 0; i < matrix.length; i++) {
			for(int j = 0; j < matrix[i].length; j++) {
				if(j == 0 && !matrix[i][j].startsWith("*")) {
					repres += ".";
				}
				
				if(j == 0 && i > 1 && !matrix[i][j].startsWith("*->")) {
					repres += "..";
				} else if(j == 0 && !matrix[i][j].startsWith("*->") && !matrix[i][j].startsWith("->")) {
					repres += "...";
				}
				
				repres += matrix[i][j] + "\t";
			}
			repres += "\n";
		}
		return repres;
	}

	// tools

	public boolean reconizeWord(String word) {
		State q = initialState;
		StateSet finalWordStates = new StateSet();
		finalWordStates = reconizeSnippet(word, q, finalWordStates);
		if (finalWordStates == null) return false;
		if (!finalWordStates.isEmpty()) {
			return true;
		}
		return false;
	}

	private StateSet reconizeSnippet(String substr, State q, StateSet finalWordStates) {
		if (substr.isEmpty()) {
			if (finalStates.contains(q)) {
				finalWordStates.add(q);
			}
			return finalWordStates;
		}
		char[] letters = substr.toCharArray();
		if (!alphabet.contains(letters[0])) return null;
		Pair<State, Character> stateAlphabet = new Pair<>(q, letters[0]);
		String newString = substr.substring(1);
		StateSet states = transitions.get(stateAlphabet);
		stateAlphabet = null;
		for (State state : states) {
			if (finalWordStates.isEmpty()) {
				reconizeSnippet(newString, state, finalWordStates);
			}
		}
		return finalWordStates;
	}

	public LinkedList<String> allSentencesFrom0ToNThatFARecognize(int n) {
		return new FASentencesGenerator(alphabet, this).allSentencesFrom0ToNThatFARecognize(n);
	}
}