package com.schiar.formaisLib.model;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;

import com.schiar.formaisLib.model.State.Type;

public class StateSet extends HashSet<State> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5193529157775251972L;

	public StateSet() {
		super();
	}
	
	public StateSet(int capacity) {
		super(capacity);
	}
	
	public StateSet(Collection<? extends State> stateCollection) {
		super(stateCollection);
	}
	
	@Override
	public String toString() {
		if (isEmpty()) {
			return "";
		}
		
		StringBuilder string = new StringBuilder();
        StateList states = sortedList();
        for (int i = 0; i < states.size()-1; i++) {
            string.append(states.get(i).toString());
            string.append(",");
        }
        string.append(states.get(states.size()-1).toString());
		return string.toString();
	}
	
	public boolean isUnitary() {
		return size() == 1;
	}
	
	public State first() {
		return iterator().next();
	}
	
	public StateList sortedList() {
		StateList stateList = new StateList(this);
		Collections.sort(stateList);
		return stateList;
	}
	
	public StateSet copy() {
		StateSet copy = new StateSet(size());
		for (State state : this) {
			copy.add(new State(state));
		}
		return copy;
	}
	
	public StateSet union(StateSet other) {
		StateSet copy = copy();
		StateSet otherCopy = other.copy();
		copy.addAll(otherCopy);
		return copy;
	}
	
	public State findStateByName(String name) {
		for(State state : this) {
			if(state.getName().equals(name)) {
				return state;
			}
		}
		return null;
	}
	
	public State joinStates() {
		Type type = Type.REGULAR;
		String newStateName = joinStateNames();
		for (State state : this) {
			if(state.isAccept()){
				type = Type.ACCEPT;
				break;
			}
		}
		return new State(newStateName, type);
	}
	
	public String joinStateNames() {	
		if (isUnitary()) return first().toString();

		StateList stateList = sortedList();
		
		StringBuilder newStateName = new StringBuilder();
		newStateName.append("{");
		for(int i = 0; i < stateList.size()-1; i++) {
			State state = stateList.get(i);
			newStateName.append(state.getName());
			newStateName.append(",");
		}
		newStateName.append(stateList.get(stateList.size()-1).getName());
		newStateName.append("}");
		
		return newStateName.toString();
	}
	
	@Override
	public boolean contains(Object object) {
		if (object == null || !(object instanceof State)) {
			return false;
		}
		
		State otherState = (State)object;
		for (State state : this) {
			if (state.equals(otherState)) {
				return true;
			}
		}
		return false;
	}
	
	public boolean containsStateNamed(String stateName) {
		for (State state : this) {
			if (state.getName().equals(stateName)) {
				return true;
			}
		}
		return false;
	}
	
	public void replace(State oldState, State newState) {
		if (remove(oldState)) {
			add(newState);
		}
	}	
	
	public StateSet changeInitialStatesToNotInitial() {
		StateSet newStates = new StateSet(size());
		for(State state : this) {
			State q;
			if(state.getType() == Type.INITIAL) {
				q = new State(state.getName());
			} else if(state.getType() == Type.ACCEPT_INITIAL) {
				q = new State(state.getName(), Type.ACCEPT);
			} else {
				q = new State(state);
			}
			newStates.add(q);
		}
		
		return newStates;
	}
	
}
