package com.schiar.formaisLib.model.regex;

import java.util.UUID;

public class RegexElement {

	public enum Type {
		TWO_OPERAND_OPERATOR(2),
		ONE_OPERAND_OPERATOR(1),
		OPERAND(0);
		
		private int operandCount;
		
		Type(int operandCount) {
			this.operandCount = operandCount;
		}
		
		public int getOperandCount() {
			return operandCount;
		}
	}
	
	private Type type;
	private Character element;
	private String id;
	
	public RegexElement(Character element) {
    	this.id = UUID.randomUUID().toString();
		this.element = element;
		
		if (isOperand()) {
			type = Type.OPERAND;
		} else if (isUnaryOperator()) {
			type = Type.ONE_OPERAND_OPERATOR;
		} else {
			type = Type.TWO_OPERAND_OPERATOR;
		}
	}
	
	public int getOperandCount() {
		return type.getOperandCount();
	}
	
	public Character getElement() {
		return element;
	}
	
	public boolean isUnaryOperator() {
		return element == '*' || element == '?';
	}

	public boolean isOperand() {
		return element != '*' && element != '?' && element != '|' && element != '.';
	}
	
	@Override
	public String toString() {
		return String.valueOf(element);
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null || !(obj instanceof RegexElement)) {
			return false;
		}
		
		RegexElement other = (RegexElement)obj;
		return id.equals(other.id) && element.equals(other.element) && type == other.type;
	}
	
}
