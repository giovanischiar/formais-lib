package com.schiar.formaisLib.model.regex;

import com.schiar.formaisLib.util.Node;

public class RegexTree {
	
	private Node<RegexElement> root;
	
	public RegexTree(Regex polishNotationRegex) {
		this(polishNotationRegex.getExpression());
	}
	
	public RegexTree(String polishNotationRegex) {
		RegexElement rootElement = new RegexElement(polishNotationRegex.charAt(0));
		root = new Node<>(rootElement, rootElement.getOperandCount());
		populateTree(root, 1, polishNotationRegex);
	}
	
	private int populateTree(Node<RegexElement> currentNode, int currentIndex, String regex) {
		if (currentIndex >= regex.length()) {
			return currentIndex;
		}
		
		int newIndex = currentIndex;
		while (currentNode.hasSpaceForMoreChildren()) {
			RegexElement element = new RegexElement(regex.charAt(newIndex));
			Node<RegexElement> child = currentNode.addChild(element, element.getOperandCount());
			newIndex++;
			newIndex = populateTree(child, newIndex, regex);
		}
		return newIndex;
	}
	
	public Node<RegexElement> getRoot() {
		return root;
	}

	@Override
	public String toString() {
		 return (root != null ? root.toString() : "");
	}

	public boolean isComplete() {
		String polishNotationRegex = getPolishNotationRegex();
		return polishNotationRegex != null && polishNotationRegex.length() > 0;
	}
	
	public String getPolishNotationRegex() {
		StringBuilder regexBuilder = new StringBuilder();
		boolean success = buildPolishNotationRegex(root, regexBuilder);
		if (!success) {
			return null;
		}
		
		return regexBuilder.toString();
	}
	
	private boolean buildPolishNotationRegex(Node<RegexElement> node, StringBuilder regexBuilder) {		
		RegexElement element = node.getElement();
		if (element == null) {
			return false;
		}
		
		regexBuilder.append(element.getElement());
		
		if (node.getChildCount() == 0) {
			return true;
		}
		
		for (int i = 0; i < node.getChildCount(); i++) {
			if (node.getChildAt(i) == null) {
				return false;
			}
			
			if (!buildPolishNotationRegex(node.getChildAt(i), regexBuilder)) {
				return false;
			}
		}
		
		return true;
	}
	
}
