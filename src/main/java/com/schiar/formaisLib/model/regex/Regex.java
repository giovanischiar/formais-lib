package com.schiar.formaisLib.model.regex;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

public class Regex {

	private String id;
	private String name;
	private String description;
	private String expression;
	
	public Regex() {
		this("", "", "");
	}
	
	public Regex(String expression) {
		this("", "", expression);
	}
	
	public Regex(String name, String description, String expression) {
    	this.id = UUID.randomUUID().toString();
		this.name = name;
		this.description = description;
		this.expression = expression;
		sanitizeExpression();
	}
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public String getExpression() {
		return expression;
	}

	public String getCanonicalExpression() {
		if (expression == null) {
			return null;
		}
		
		String revisedExpression = expression;
		List<Character> operands = getListOfOperands();
		for (Character character : operands) {
			revisedExpression = revisedExpression.replaceAll(character + "\\+", character + "." + character + "*");
		}
	
		StringBuilder newExpression = new StringBuilder();
		for (int i = 0; i < revisedExpression.length()-1; i++) {
			char current = revisedExpression.charAt(i);
			newExpression.append(current);
			
			char next = revisedExpression.charAt(i+1);
			if ((isUnaryOperator(current) || isOperand(current) || isRightParenthesis(current)) && (isOperand(next) || isLeftParenthesis(next))) {
				newExpression.append('.');
			}
		}
		newExpression.append(revisedExpression.charAt(revisedExpression.length()-1));
		return newExpression.toString();
	} 
	
	public List<Character> getListOfOperands() {
		Set<Character> operands = new HashSet<>();
		if (expression == null) {
			return new ArrayList<>(operands);
		}
		
		for (int i = 0; i < expression.length(); i++) {
			char character = expression.charAt(i);
			if (isOperand(character)) {
				operands.add(character);
			}
		}
		return new ArrayList<>(operands);
	}
	
	private void sanitizeExpression() {
		expression = expression.replaceAll("[^a-z0-9()*?|.+]+", "");
	}
	
	private static boolean isUnaryOperator(char c) {
		return c == '*' || c == '?' || c == '+';
	}
	
	private static boolean isOperand(char c) {
		return c != '(' && c != ')' && c != '*' && c != '?' && c != '|' && c != '.' && c != '+';
	}
	
	private static boolean isLeftParenthesis(char c) {
		return c == '(';
	}
	
	private static boolean isRightParenthesis(char c) {
		return c == ')';
	}

}
