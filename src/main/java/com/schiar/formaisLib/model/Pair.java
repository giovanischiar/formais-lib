package com.schiar.formaisLib.model;

public class Pair<T, U> {
	
	private T _first;
	private U _second;
	
	public Pair(T first, U second) {
		_first = first;
		_second = second;
	}
	
	public Pair(Pair<T, U> pair) {
		_first = pair._first;
		_second = pair._second;
	}
	
	public T first() {
		return _first;
	}
	
	public U second() {
		return _second;
	}

	public String toString() {
		return "(" + _first.toString() + ", " + _second.toString() + ")";
	}
	
	@Override
	public int hashCode(){
	    return (this.toString()).hashCode();
	}

	@Override
	public boolean equals(Object o){
	    return (this.toString()).equals(o.toString());
	}
}
