package com.schiar.formaisLib.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.schiar.formaisLib.model.FiniteAutomaton;

public class TransitionTableVerifier {
	
	private String[][] table;
	private List<String> states;
	private List<Error> errors;
	private boolean ndfa;
	
	public TransitionTableVerifier(String[][] table, boolean ndfa) {
		this.table = table;
		this.ndfa = ndfa;
		extractStates();
		errors = new ArrayList<>();
		verifyEmptyEntry();
		verifyRepeatedAlphabet();
		verifyWithoutInitialState();
		vefifyTheresUndefinedStates();
		verifyRepeatedStates();
	}
	
	private void extractStates() {
        states = new ArrayList<>();
        for(int i = 1; i < table.length; i++) {
            String rawState = table[i][0];
            if(rawState.startsWith("->")) {
                String state = rawState.replace("->", "");
                states.add(state);
                continue;
            }
            if(rawState.startsWith("*->")) {
                String state = rawState.replace("*->", "");
                states.add(state);
                continue;
            }
            if(rawState.startsWith("*")) {
                String state = rawState.replace("*", "");
                states.add(state);
                continue;
            }
            states.add(rawState);
        }
    }
	
	private void verifyEmptyEntry() {
		for(int i = 0; i < table.length; i++) {
			for(int j = 0; j < table[0].length; j++) {
				if(table[i][j].equals("")) {
					errors.add(Error.EMPTYENTRY);
					return;
				}
			}
		}
	}	
	
	private void verifyWithoutInitialState() {
		for(int i = 1; i < table.length; i++) {
			String state = table[i][0];
			if(state.startsWith("->") ||
			   state.startsWith("*->")) {
				return;
			}
		}
		errors.add(Error.WITHOUTINITIALSTATE);
	}
	
	private void vefifyTheresUndefinedStates() {
		for(int i = 1; i < table.length; i++) {
			for(int j = 1; j < table[0].length; j++) {
				List<String> possiblyStates = new ArrayList<String>();
				if(!ndfa) {
					possiblyStates.add(table[i][j]);
				} else {
					possiblyStates = Arrays.asList(table[i][j].split(","));
				}
				for(String state : possiblyStates) {
					if(!states.contains(state) && !state.equals(FiniteAutomaton.TRAPSTATE)) {
						errors.add(Error.UNDEFINEDSTATES);
						return;
					}
				}
			}
		}
	}
	
	private void verifyRepeatedStates() {
		for(int i = 0; i < states.size(); i++) {
			String state = states.get(i);
			for(int j = 0; j < states.size(); j++) {
				if(i == j) continue;
				if(state.equals(states.get(j))) { 
					errors.add(Error.REPEATSTATES);
					return;
				}
			}
		}
	}
	
	private void verifyRepeatedAlphabet() {
		for(int i = 1; i < table[0].length; i++) {
			String letter = table[0][i];
			for(int j = 1; j < table[0].length; j++) {
				if(i == j) continue;
				if(letter.equals(table[0][j])) { 
					errors.add(Error.REPEATALPHABET);
					return;
				}
			}
		}
	}
	
	public List<Error> getErrors() {
		return errors;
	}
}
