package com.schiar.formaisLib.util;

public enum Error {
	EMPTYENTRY,
	WITHOUTINITIALSTATE, 
	REPEATSTATES,
	UNDEFINEDSTATES,
	REPEATALPHABET
}
