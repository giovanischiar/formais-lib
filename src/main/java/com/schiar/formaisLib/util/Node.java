package com.schiar.formaisLib.util;

public class Node<E> {
	
	private E element;
	private Node<E>[] children;

	@SuppressWarnings("unchecked")
	public Node(E element, int childrenCount) {
		this.element = element;
		if (childrenCount > 0) {
			this.children = new Node[childrenCount];
		}
	}
	
	public E getElement() {
		return element;
	}
	
	public int getChildCount() {
		return children == null ? 0 : children.length;
	}
	
	public Node<E> getChildAt(int index) {
		return children[index];
	}

	public Node<E>[] getChildren() {
		return children;
	}

	public Node<E> addChild(E element, int childrenCount) {
		if (children == null) {
			throw new IllegalStateException("Element " + element + "can't have children.");
		}
		
		for (int i = 0; i < children.length; i++) {
			if (children[i] == null) {
				children[i] = new Node<E>(element, childrenCount);
				return children[i];
			}
		}
		
		throw new IllegalStateException("Element " + element + "can't have more children.");
	}
	
	public boolean hasSpaceForMoreChildren() {
		if (children == null) {
			return false;
		}
		
		for (int i = 0; i < children.length; i++) {
			if (children[i] == null) {
				return true;
			}
		}
		
		return false;
	}
	
	@Override
	public String toString() {
		String childrenString = "";
		if (children != null) {
			StringBuilder childrenBuilder = new StringBuilder();
			for (int i = 0; i < children.length; i++) {
				childrenBuilder.append("{" + children[i].toString() + "}");
				if (i < children.length-1) {
					childrenBuilder.append(", ");
				}
			}
			childrenString = childrenBuilder.toString();
		}
		return element.toString() + " -> " + childrenString;
	}
	
}
