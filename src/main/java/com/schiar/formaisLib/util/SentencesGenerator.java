package com.schiar.formaisLib.util;

import java.util.LinkedList;

import com.schiar.formaisLib.model.Alphabet;

public class SentencesGenerator {
	
	private Alphabet alphabet;
	
	public SentencesGenerator(Alphabet alphabet) {
		this.alphabet = alphabet;
	}
	
    public LinkedList<String> allSentencesFrom0ToN(int n) {
    	LinkedList<String> sentences = new LinkedList<>();
    	sentences.add("");
    	for(int i = 1; i <= n; i++) {
    		sentences.addAll(allNLengthSentences(i));
    	}
    	return sentences;
    }
    
    private LinkedList<String> allNLengthSentences(int n) {
    	Character[] currentCharacters = new Character[n];
    	LinkedList<String> sentences = new LinkedList<>();
    	sentences = allNLengthSentencesR(0, currentCharacters, sentences);
    	return sentences;
    }
    
    private LinkedList<String> allNLengthSentencesR(int index, Character[] currentCharacters, LinkedList<String> sentences) {
    	for(char character : alphabet) {
    		currentCharacters[index] = character;
    		if(index < currentCharacters.length-1) {
    			allNLengthSentencesR(index+1, currentCharacters, sentences);
    		} else {
    			createSentence(currentCharacters, sentences);
    		}
    	}
    	return sentences;
    }
    
    private void createSentence(Character[] currentCharacters, LinkedList<String> sentences) {
    	StringBuilder sentence = new StringBuilder();
    	for(char character : currentCharacters) {
    		sentence.append(character);
    	}
		sentences.add(sentence.toString());
    }
}
